﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class AIAbilityCommand : AICommand {

    private Ability ability;

    public AIAbilityCommand(GameController gameController, AiInput aiInput, BoardData data, Character unit, float s, Vector2 destination, Ability skill) :
        base(gameController, aiInput, data, unit, s, destination) {
        ability = Object.Instantiate(skill);
    }

    public override IEnumerator Execute() {

        yield return new WaitForSecondsRealtime(speed);
        input.MousePosition = character.Position;
        yield return new WaitForSecondsRealtime(speed);
        input.MouseButton0 = true;
        yield return new WaitForSecondsRealtime(speed);
        controller.SetSelectedAbility(ability.Name);
        yield return new WaitForSecondsRealtime(speed);
        input.MousePosition = target;
        yield return new WaitForSecondsRealtime(speed);
        input.MouseButton0 = true;
        yield return new WaitForSecondsRealtime(speed);
        input.MouseButton1 = true;
        yield return null;
        SimulateExecution();

    }

    public override void SimulateExecution() {
        character.ActionPoints -= ability.Cost;
        character.Selected = true;
        ability.Use(ability.TargetingComponent.GetRedBuffer(target, character, aiData).Distinct().ToList(), character, aiData);
        character.Selected = false;
    }

    public override void Undo() {
        //Debug.Log(ability.Name);

        ability.Undo();
        character.ActionPoints += ability.Cost;
    }

    public override string ToString() {
        return ability.Name + " : ";
    }

}