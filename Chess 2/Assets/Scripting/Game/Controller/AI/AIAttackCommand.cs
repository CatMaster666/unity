﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;


public class AIAttackCommand : AICommand {

    private Ability basic;

    public AIAttackCommand(GameController gameController, AiInput aiInput, BoardData data, Character unit, float s, Vector2 destination) :
        base(gameController, aiInput, data, unit, s, destination) {
        basic = Object.Instantiate(character.BasicAttack);
    }

    public override IEnumerator Execute() {
        yield return new WaitForSecondsRealtime(speed);
        input.MousePosition = character.Position;
        yield return new WaitForSecondsRealtime(speed);
        input.MouseButton0 = true;
        yield return new WaitForSecondsRealtime(speed);
        controller.GoToAttackMode();
        yield return new WaitForSecondsRealtime(speed);
        input.MousePosition = target;
        yield return new WaitForSecondsRealtime(speed);
        input.MouseButton0 = true;
        yield return new WaitForSecondsRealtime(speed);
        input.MouseButton1 = true;
        yield return null;
        SimulateExecution();

    }

    public override void SimulateExecution() {
        character.ActionPoints -= basic.Cost;
        character.Selected = true;
        basic.Use(basic.TargetingComponent.GetRedBuffer(target, character, aiData).Distinct().ToList(), character, aiData);
        character.Selected = false;
    }

    public override void Undo() {
        basic.Undo();
        character.ActionPoints += basic.Cost;
    }

}