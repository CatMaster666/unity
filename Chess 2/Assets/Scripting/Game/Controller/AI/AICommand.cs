﻿using System.Collections;
using UnityEngine;


public class AICommand {

    protected GameController controller;
    protected AiInput input;
    protected BoardData aiData;
    protected float speed;
    protected Character character;
    protected Vector2 target;

    public AICommand() {

    }

    public AICommand(GameController gameController, AiInput aiInput, BoardData data, Character unit, float s, Vector2 destination) {
        character = unit;
        controller = gameController;
        input = aiInput;
        aiData = data;
        speed = s;
        target = destination;
    }

    public virtual IEnumerator Execute() {
        yield return new WaitForSecondsRealtime(speed);
    }

    public virtual void SimulateExecution() {

    }

    public virtual void Undo() {

    }

}