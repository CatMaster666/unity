﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class AIController : MonoBehaviour {

    private GameController controller;
    public AiInput input;
    private float speed = 0.1f;
    private PathfindingModel pathfinder;
    private BoardData boardDataCopy = new BoardData();
    private string BoardDataSerialized;

    public void Init(GameController gameController) {
        controller = gameController;
        input = new AiInput();
    }

    public void TakeTurn() {

        BoardDataSerialized = JsonSerializer.Serialize(typeof(BoardData), ResourcesManager.Instance.Data);
        boardDataCopy = (BoardData) JsonSerializer.Deserialize(typeof(BoardData), BoardDataSerialized);
        pathfinder = new PathfindingModel(boardDataCopy);

        StartCoroutine(Turn());
    }

    private IEnumerator Turn() {

        foreach (Character character in boardDataCopy.Units.FindAll(x => x is Character && ((Character) x).Player == boardDataCopy.Turn)) {
            //int i = 0;
            while (true) {
                AICommand[] commands = new AICommand[2];

                IEnumerable<AICommand> possibleFirstMoves;
                IEnumerable<AICommand> possibleSecondMoves;

                IEnumerable<AICommand> possibleThirdMoves;
                int maxValue = -10000000;
                int currentValue;

                possibleFirstMoves = GetPossibleMoves(character, true);

                foreach (AICommand possibleFirstMove in possibleFirstMoves) {
                    possibleFirstMove.SimulateExecution();
                    possibleSecondMoves = GetPossibleMoves(character, true);

                    //yield return null;
                    foreach (AICommand possibleSecondMove in possibleSecondMoves) {
                        possibleSecondMove.SimulateExecution();

                        //possibleThirdMoves = GetPossibleMoves(character, false);

                        //foreach (AICommand possibeThirdMove in possibleThirdMoves) {
                        //    possibeThirdMove.SimulateExecution();
                        currentValue = CalculateValue();
                        if (currentValue > maxValue) {
                            maxValue = currentValue;
                            commands[0] = possibleFirstMove;
                            commands[1] = possibleSecondMove;

                            //commands[2] = possibeThirdMove;
                        }

                        // possibeThirdMove.Undo();
                        //}

                        possibleSecondMove.Undo();
                    }
                    possibleFirstMove.Undo();
                }

                foreach (AICommand t in commands) {
                    t.SimulateExecution();
                }
                if (commands.Any(c => c is AIWaitCommand)) {
                    break;
                }

                //i++;
            }

        }
        Stop();
        yield return null;
    }

    private void Search(Character character, int currentDepth, int maxDepth, int maxValue, List<AICommand> currentPath, AICommand[] outInstructions) {
        List<AICommand> possibleMoves = GetPossibleMoves(character, true);
        foreach (AICommand possibleMove in possibleMoves) {
            possibleMove.SimulateExecution();
            currentPath[currentDepth] = possibleMove;
            if (currentDepth == maxDepth) {
                int currentValue = CalculateValue();
                if (currentValue > maxValue) {
                    currentPath.CopyTo(outInstructions);
                }
            }
            else {
                Search(character, currentDepth + 1, maxDepth, maxValue, currentPath, outInstructions);
            }

        }
    }

    private void Stop() {
        controller.GoToEndOfTurnMode();

    }

    private IEnumerator ExecuteActions(AICommand[] commands) {
        foreach (AICommand c in commands) {
            yield return StartCoroutine(c.Execute());
        }
    }

    private int CalculateValue() {
        int value = 0;
        foreach (Character character in boardDataCopy.Units.OfType<Character>()) {
            if (character.Player == boardDataCopy.Turn) {
                value += character.ActionPoints;
                value += character.HitPoints;
            }
            else {
                value -= character.HitPoints;
            }
        }
        return value;
    }

    private List<AICommand> GetPossibleMoves(Character character, bool considerMovement) {
        List<AICommand> buffer = new List<AICommand>();

        if (character != null) {
            if (!character.AppliedEffects.Exists(x => x.EffectTag == DurationEffect.DurationEffectTag.Stuned)) {
                if (considerMovement) {
                    if (character.ActionPoints >= 1 && character.Statistics[StatisticType.MovementRange].Value >= 1) {
                        pathfinder.CalculatePathfindingGraph(character.Position);
                        if (pathfinder.GetValidPaths(character.Statistics[StatisticType.MovementRange].Value).Count > 0) {
                            foreach (Vector2 position in pathfinder.GetValidPaths(character.Statistics[StatisticType.MovementRange].Value).Distinct()) {
                                buffer.Add(new AIMoveCommand(controller, input, boardDataCopy, character, speed, position));
                            }
                        }
                    }
                }

                foreach (Ability ability in character.AvailibleAbilities) {
                    if (character.ActionPoints >= ability.Cost) {
                        List<Vector2> validTargets = ability.TargetingComponent.GetGrayBuffer(boardDataCopy, character).Distinct().ToList().FindAll(x => ability.TargetingComponent.IsTargetValid(x, character, true, boardDataCopy));
                        for (int index = 0; index < validTargets.Count; index++) {

                            buffer.Add(new AIAbilityCommand(controller, input, boardDataCopy, character, speed, validTargets[index], ability));
                        }
                    }
                }
                if (character.ActionPoints >= character.BasicAttack.Cost) {
                    List<Vector2> validTargets = character.BasicAttack.TargetingComponent.GetGrayBuffer(boardDataCopy, character).Distinct().ToList().FindAll(x => character.BasicAttack.TargetingComponent.IsTargetValid(x, character, true, boardDataCopy));
                    for (int index = 0; index < validTargets.Count; index++) {

                        buffer.Add(new AIAttackCommand(controller, input, boardDataCopy, character, speed, validTargets[index]));
                    }
                }

            }
            buffer.Add(new AIWaitCommand(speed));
        }
        return buffer;
    }

}