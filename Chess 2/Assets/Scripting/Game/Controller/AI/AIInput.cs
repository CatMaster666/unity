﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AiInput {
    public bool MouseButton0;
    public bool MouseButton1;
    public Vector2 MousePosition;
}