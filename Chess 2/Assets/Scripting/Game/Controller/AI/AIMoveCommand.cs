﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AIMoveCommand : AICommand {

    private Vector2 startPosition;

    public AIMoveCommand(GameController gameController, AiInput aiInput, BoardData data, Character unit, float s, Vector2 destination) : base(gameController, aiInput, data, unit, s, destination) {
        startPosition = character.Position;
    }

    public override IEnumerator Execute() {
        yield return new WaitForSecondsRealtime(speed);
        input.MousePosition = character.Position;
        yield return new WaitForSecondsRealtime(speed);
        input.MouseButton0 = true;
        yield return new WaitForSecondsRealtime(speed);
        controller.GoToMovementMode();
        yield return new WaitForSecondsRealtime(speed);
        input.MousePosition = target;
        yield return new WaitForSecondsRealtime(speed);
        input.MouseButton0 = true;
        yield return new WaitForSecondsRealtime(speed);
        input.MouseButton1 = true;
        yield return null;
        SimulateExecution();

    }

    public override void SimulateExecution() {
        character.Position = target;
        character.ActionPoints--;
    }

    public override void Undo() {
        character.Position = startPosition;
        character.ActionPoints++;
    }

}