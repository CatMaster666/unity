﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class GameController : MonoBehaviour {

    [SerializeField] private AIController ai;
    private ViewControllerMediatior mediator;
    private ViewHelper viewHelper;
    private PathfindingModel pathfinder;

    private Vector2 mousePosition;
    private Vector2 FreshMousePosition;
    private Character selectedCharacter;
    private bool didFirstRefresh;
    private bool isControlledByAI = true;
    private bool isLeftMouseButtonDown;
    private bool isRightMouseButtonDown;
    private bool wasGraveyardUpdated;
    private int lastGraveyardCount;
    private List<Unit> unitsToDelete = new List<Unit>();

    private void Start() {
        ResourcesManager.Instance.Data.GameState = State.SelectingUnit;
        pathfinder = new PathfindingModel(ResourcesManager.Instance.Data);
        viewHelper = new ViewHelper(pathfinder);
        mediator = new ViewControllerMediatior(viewHelper);
        ai.Init(this);
    }

    private void Update() {

        if (ResourcesManager.Instance.Data.Graveyard.Count > lastGraveyardCount) {
            lastGraveyardCount = ResourcesManager.Instance.Data.Graveyard.Count;
            wasGraveyardUpdated = true;
        }

        //check if input is driven by player or ai
        FreshMousePosition = isControlledByAI
            ? ai.input.MousePosition
            : BoardData.MouseToBoardPosition();
        isLeftMouseButtonDown = isControlledByAI
            ? ai.input.MouseButton0
            : Input.GetMouseButtonDown(0);
        isRightMouseButtonDown = isControlledByAI
            ? ai.input.MouseButton1
            : Input.GetMouseButtonDown(1);

        //redraw view when loaded first time
        if (!didFirstRefresh) {
            mediator.ClearAndUpdateAll();
            didFirstRefresh = true;
        }

        selectedCharacter = ResourcesManager.Instance.Data.GetSelecetedUnit();

        //if mouse position changed, ask mediator to update appropriate elements of view
        if (mousePosition != FreshMousePosition) {
            mousePosition = FreshMousePosition;
            mediator.MouseMoved(mousePosition);
        }

        if (isLeftMouseButtonDown) {

            ai.input.MouseButton0 = false;
            if (ResourcesManager.Instance.Data.GameState == State.SelectingUnit) {
                TrySelect();
            }

            //MouseToBoardPosition returns Vector2(1000,1000) when mouse is not above the board
            if (mousePosition != new Vector2(1000, 1000)) {
                if (ResourcesManager.Instance.Data.GameState == State.Movement) {
                    TryMove();
                }
                if (ResourcesManager.Instance.Data.GameState == State.Cast) {
                    TryCast();
                }
            }
        }

        if (isRightMouseButtonDown) {
            ai.input.MouseButton1 = false;
            DeselectUnits();
        }
        DestroyDeadUnits();
        CorrectHitPoints();
        if (ResourcesManager.Instance.Data.GameState == State.EndOfTurn) {
            Debug.Log("End turn");
            EndTurn();
        }

    }

    ////////////////////////////////////////////METHODS USED IN UPDATE////////////////////////////////////////////////

    private void TrySelect() {
        Character target = ResourcesManager.Instance.Data.GetCharacterAt(mousePosition);
        if (target != null && target.Player == ResourcesManager.Instance.Data.Turn) {
            ResourcesManager.Instance.Data.SelectCharacter(target);
            mediator.Select();
        }
    }

    private void TryMove() {
        if (pathfinder.GetPath(mousePosition, selectedCharacter.Statistics[StatisticType.MovementRange].Value).Count > 0 && mousePosition != selectedCharacter.Position) {
            if (selectedCharacter.ActionPoints >= 1) {
                selectedCharacter.ActionPoints -= 1;
                selectedCharacter.Position = mousePosition;
                mediator.Move();
                if (selectedCharacter.ActionPoints < 1) {
                    DeselectUnits();
                }
            }
        }
    }

    private void TryCast() {

        List<Vector2> affectedFields = viewHelper.RedFields.Distinct().ToList();

        if (selectedCharacter.ActionPoints >= selectedCharacter.SelectedAbility.Cost) {
            if (affectedFields.Contains(mousePosition)) {
                if (selectedCharacter.SelectedAbility.TargetingComponent.IsTargetValid(mousePosition, selectedCharacter, false, ResourcesManager.Instance.Data)) {
                    selectedCharacter.ActionPoints -= selectedCharacter.SelectedAbility.Cost;
                    selectedCharacter.SelectedAbility.Use(affectedFields, selectedCharacter, ResourcesManager.Instance.Data);
                    Debug.Log(selectedCharacter.SelectedAbility.Name);
                    mediator.Cast(selectedCharacter.SelectedAbility, mousePosition);
                    if (selectedCharacter.ActionPoints < selectedCharacter.SelectedAbility.Cost) {
                        DeselectUnits();
                    }
                }
            }
        }
    }

    private void DeselectUnits() {
        ResourcesManager.Instance.Data.DeselectUnits();
        ResourcesManager.Instance.Data.GameState = State.SelectingUnit;
        mediator.Clear();
    }

    private void EndTurn() {
        ResourcesManager.Instance.Data.DeselectUnits();
        for (int index = 0; index < ResourcesManager.Instance.Data.Units.Count; index++) {
            Unit unit = ResourcesManager.Instance.Data.Units[index];
            unit.OnTurnEnd();
        }

        ResourcesManager.Instance.Data.Turn = !ResourcesManager.Instance.Data.Turn;
        ResourcesManager.Instance.Data.GameState = State.SelectingUnit;
        mediator.ClearAndUpdateAll();

        //isControlledByAI = !isControlledByAI;
        if (isControlledByAI) {

            ai.TakeTurn();
        }
    }

    private void DestroyDeadUnits() {
        unitsToDelete.Clear();
        unitsToDelete.AddRange(ResourcesManager.Instance.Data.Units.FindAll(x => x.IsSummon && x.Duration <= 0));

        //unitsToDelete.AddRange(ResourcesManager.Instance.Data.Units.FindAll(x => x is Character && ((Character) x).HitPoints <= 0));
        //ResourcesManager.Instance.Data.Units.RemoveAll(item => unitsToDelete.Contains(item));
        if (unitsToDelete.Count > 0 || wasGraveyardUpdated) {
            if (ResourcesManager.Instance.Data.GameState == State.Cast || ResourcesManager.Instance.Data.GameState == State.Attack) {
                if (ResourcesManager.Instance.Data.GetSelecetedUnit() == null) {
                    mediator.ClearAndUpdateAll();
                }
                else {
                    mediator.Cast(ResourcesManager.Instance.Data.GetSelecetedUnit().SelectedAbility, mousePosition);
                }
            }
            else {
                mediator.ClearAndUpdateAll();
            }
            wasGraveyardUpdated = false;
        }
    }

    private void CorrectHitPoints() {
        foreach (Character character in ResourcesManager.Instance.Data.Units.OfType<Character>()) {
            if (character.HitPoints > character.Statistics[StatisticType.MaxHitPoints].Value) {
                character.HitPoints = character.Statistics[StatisticType.MaxHitPoints].Value;
                mediator.MouseMoved(new Vector2(1000, 1000));
            }
        }
    }

    ////////////////////////////////////////////////INPUT FROM UI BUTTONS//////////////////////////////////////////////////////

    public void GoToEndOfTurnMode() {

        ResourcesManager.Instance.Data.GameState = State.EndOfTurn;

        //InGameConsole.AddMessage("---END TURN---");
    }

    public void GoToAttackMode() {
        ResourcesManager.Instance.Data.GetSelecetedUnit().SelectedAbility = ResourcesManager.Instance.Data.GetSelecetedUnit().BasicAttack;
        ResourcesManager.Instance.Data.GameState = State.Cast;
        mediator.Clear();
        mediator.Cast(ResourcesManager.Instance.Data.GetSelecetedUnit().BasicAttack, new Vector2(1000, 1000));
    }

    public void GoToMovementMode() {
        ResourcesManager.Instance.Data.GameState = State.Movement;
        mediator.Clear();
        mediator.Move();
    }

    public void GoToSpellSelectionMode() {
        ResourcesManager.Instance.Data.GameState = State.SelectingAbility;
        mediator.Clear();
        mediator.Select();
    }

    public void SetSelectedAbility(string ability) {
        Character selected = ResourcesManager.Instance.Data.GetSelecetedUnit();
        if (selected != null && ability != string.Empty) {
            selected.SelectedAbility = selected.AvailibleAbilities.Find(x => x.Name == ability);
            mediator.Cast(selected.SelectedAbility, FreshMousePosition);
            ResourcesManager.Instance.Data.GameState = State.Cast;
        }
    }

}