﻿using UnityEngine;
using UnityEngine.AI;


public class ViewControllerMediatior {

    private BoardView boardView;
    private ViewHelper helper;
    private UIView uiView;

    public ViewControllerMediatior(ViewHelper viewHelper) {
        GameObject viewObject = GameObject.Find("View");
        boardView = viewObject.GetComponent<BoardView>();
        uiView = viewObject.GetComponent<UIView>();
        helper = viewHelper;
        boardView.Init(helper);
    }

    public void ClearAndUpdateAll() {
        Clear();
        boardView.UpdateTags();
        boardView.UpdateFieldsActionPoints();
        boardView.UpdateFieldsHitPointsBars();
    }

    public void Select() {
        boardView.UpdateColors();
        uiView.UpdateUnitUi();
        uiView.UpdateDescryption();
    }

    public void Move() {
        helper.YellowFields.Clear();
        helper.CalculateGrayMovement();
        boardView.UpdateTags();
        boardView.UpdateFieldsActionPoints();
        boardView.UpdateColors();
        uiView.UpdateDescryption();
        boardView.UpdateFieldsHitPointsBars();
    }

    public void Cast(Ability ability, Vector2 mousePosition) {
        helper.CalculateGrayAbility(ability,ResourcesManager.Instance.Data.GetSelecetedUnit());
        if (mousePosition != new Vector2(1000, 1000)) {
            boardView.UpdateTags();
            boardView.UpdateFieldsActionPoints();
            uiView.UpdateUnitUi();
            helper.CalculateRedAbility(ability,ResourcesManager.Instance.Data.GetSelecetedUnit(), mousePosition);
            boardView.UpdateFieldsHitPointsBars();
            uiView.UpdateDescryption();
        }

        boardView.UpdateColors();
    }

    public void Clear() {
        helper.YellowFields.Clear();
        helper.GreyFields.Clear();
        helper.RedFields.Clear();
        uiView.UpdateUnitUi();
        uiView.UpdateDescryption();
        boardView.UpdateColors();

    }

    public void MouseMoved(Vector2 mousePosition) {
        if (ResourcesManager.Instance.Data.GameState == State.Movement) {
            helper.CalculateYellowMovement(mousePosition);
            boardView.UpdateColors();
        }

        if (ResourcesManager.Instance.Data.GameState == State.Attack ||
            ResourcesManager.Instance.Data.GameState == State.Cast) {
            if (ResourcesManager.Instance.Data.GetSelecetedUnit() != null) {
                helper.CalculateRedAbility(ResourcesManager.Instance.Data.GetSelecetedUnit().SelectedAbility, ResourcesManager.Instance.Data.GetSelecetedUnit(),mousePosition);
            }
            boardView.UpdateColors();
        }
        boardView.UpdateFieldsCursor(mousePosition);
        uiView.UpdateDescryption();
    }

}