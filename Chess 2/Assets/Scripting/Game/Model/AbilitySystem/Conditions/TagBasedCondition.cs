﻿using UnityEngine;


[CreateAssetMenu]
public class TagBasedCondition : Condition {

    [Header("Check exacly one option")] [SerializeField]
    private bool casterHasTag;
    [SerializeField] private bool targetHasTag;
    [SerializeField] private DurationEffect.DurationEffectTag Tag;

    public override bool TestPassed(Character caster, Vector2 target, BoardData usedData) {

        bool passed = false;

        if (casterHasTag) {
            if (caster.AppliedEffects.Exists(x => x.EffectTag == Tag)) {
                passed = true;
            }
            else {
                passed = false;
            }
        }
        if (targetHasTag) {
            if (usedData.GetCharacterAt(target).AppliedEffects.Exists(x => x.EffectTag == Tag)) {
                passed = true;
            }
            else {
                passed = false;
            }
        }

        return passed;

    }

}