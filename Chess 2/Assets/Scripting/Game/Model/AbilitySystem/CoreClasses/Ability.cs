﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;


[CreateAssetMenu]
[Serializable]
public class Ability : ScriptableObject {

    [Header("Main properties")] [Space(order = 1)]
    public string Name;
    [TextArea] public string Descryption;
    public int Cost;
    [SerializeField] public TargetingComponent TargetingComponent;
    [Header("Resistances")] [SerializeField]
    private bool IsMagicAbility;
    [SerializeField] private bool IgnoreMagicResistance;
    [Header("Effects")] [SerializeField] public List<Effect> TargetedEffects;
    [SerializeField] public List<Effect> SelfTargetingEffects;

    private BoardData data;

    public void Use(List<Vector2> targets, Character user, BoardData usedData) {

        data = usedData;
        targets = targets.Where(x => TargetingComponent.IsTargetAffected(x, user, data) && !Resisted(x)).ToList();

        foreach (Effect effect in TargetedEffects) {
            effect.OnCast(targets, user, data);
        }

        foreach (Effect effect in SelfTargetingEffects) {
            effect.OnCast(new List<Vector2> {user.Position}, user, data);
        }
    }

    public void Undo() {

        foreach (Effect effect in TargetedEffects) {
            effect.Undo();
        }

        foreach (Effect effect in SelfTargetingEffects) {
            effect.Undo();
        }
    }

    private bool Resisted(Vector2 target) {
        Character characterAtTarget = data.GetCharacterAt(target);
        if (characterAtTarget == null) {
            return false;
        }
        if (!IsMagicAbility) {
            return false;
        }
        if (IgnoreMagicResistance) {
            return false;
        }
        if (Random.Range(0, 100) >= characterAtTarget.Statistics[StatisticType.MagicResistance].Value) {
            return false;
        }

        return true;

    }

    public string GenerateDescryption() {
        return "Name : " + Name + "\n\n" + "Cost : " + Cost + "\n" + "Range : " + TargetingComponent.Range + "\n\n" + Descryption;
    }

}