﻿using UnityEngine;


public class Condition : ScriptableObject {

    public virtual bool TestPassed(Character caster, Vector2 target,BoardData usedData) {
        return true;
    }

}