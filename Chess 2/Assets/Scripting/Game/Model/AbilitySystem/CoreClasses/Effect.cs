﻿using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public abstract class Effect : ScriptableObject {

    public BoardData data;
    public Stack<Character> castersHistory = new Stack<Character>();

    public virtual void Init() {
        castersHistory.Clear();
    }

    public virtual void OnCast(List<Vector2> targets, Character caster, BoardData usedData) {
    }

    public virtual void Undo() {
    }

}