﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;


public enum TargetingType {

    PROJECTILE,
    UNLIMITED

}


public enum ValidTargetType {

    ANY_TARGET,
    EMPTY_FIELD,
    ENEMY_UNIT,
    FRIENDLY_UNIT,
    CASTER,
    ANY_UNIT

}


public enum AreaOfEffectType {

    AOE,
    SINGLE_TARGET

}


public enum AffectedUnitsType {

    FRIENDLY_UNITS,
    ENEMY_UNITS,
    ALL_UNITS,
    EMPTY_FIELDS,
    ALL_CONSTRUCTS

}


[Serializable]
public class TargetingComponent {

    [Header("targeting rules")]
    [SerializeField]
    private TargetingType TargetingTypeRules;
    [SerializeField] private ValidTargetType ValidTargetTypeRules;
    [SerializeField] private AffectedUnitsType AffectedUnitsTypeRules;
    [SerializeField] public AreaOfEffectType AreaOfEffectTypeRules;
    [SerializeField] private bool IsCasterAffected;
    [SerializeField] private bool IsCasterValidTarget;

    [Header("ability range")] public int Range;
    [SerializeField] private int AoeRange;

    [Header("information for ai")]
    [SerializeField]
    public ValidTargetType PrefferableTargetType;

    public List<Vector2> GetGrayBuffer(BoardData usedData, Character user) {
        List<Vector2> buffer = new List<Vector2>();

        if (TargetingTypeRules == TargetingType.PROJECTILE) {
            return VisibilityRaycaster.GetVisibleTargets(user.Position, Range, usedData);
        }

        if (TargetingTypeRules == TargetingType.UNLIMITED) {
            for (int x = 0; x < BoardData.Width; x++) {
                for (int y = 0; y < BoardData.Height; y++) {
                    if (Vector2.Distance(new Vector2(x, y), user.Position) <= Range) {
                        buffer.Add(new Vector2(x, y));
                    }
                }
            }
        }

        return buffer;
    }

    public IEnumerable<Vector2> GetRedBuffer(Vector2 target, Character user, BoardData usedData) {
        if (AoeRange == 0) {
            return new List<Vector2> {target};
        }
        IEnumerable<Vector2> buffer = VisibilityRaycaster.GetVisibleTargets(target, AoeRange, usedData);

        return buffer;
    }

    public bool IsTargetValid(Vector2 target, Character user, bool usingAI, BoardData usedData) {

        Character characterAt = usedData.GetCharacterAt(target);
        if (target == user.Position && !IsCasterValidTarget) {
            return false;
        }
        ValidTargetType rules;
        rules = usingAI
            ? PrefferableTargetType
            : ValidTargetTypeRules;
        bool isTargetFree = !usedData.IsNotFree(target);
        switch (rules) {
            case ValidTargetType.ANY_TARGET:
                if (characterAt != null || isTargetFree) {
                    return true;
                }

                break;
            case ValidTargetType.CASTER:
                if (target == user.Position) {
                    return true;
                }

                break;
            case ValidTargetType.ANY_UNIT:
                if (characterAt != null) {
                    return true;
                }

                break;
            case ValidTargetType.EMPTY_FIELD:
                if (isTargetFree) {
                    return true;
                }

                break;
            case ValidTargetType.FRIENDLY_UNIT:
                if (characterAt != null) {
                    if (characterAt.Player == user.Player) {
                        return true;
                    }
                }

                break;
            case ValidTargetType.ENEMY_UNIT:
                if (characterAt != null) {
                    if (characterAt.Player != user.Player) {
                        return true;
                    }
                }

                break;
            default: throw new ArgumentOutOfRangeException();
        }

        return false;
    }

    public bool IsTargetAffected(Vector2 target, Character user, BoardData usedData) {

        Character characterAt = usedData.GetCharacterAt(target);
        bool isTargetFree = !usedData.IsNotFree(target);
        if (user.Position == target && !IsCasterAffected) {
            return false;
        }

        switch (AffectedUnitsTypeRules) {
            case AffectedUnitsType.FRIENDLY_UNITS:
                if (characterAt != null) {
                    if (characterAt.Player == user.Player) {
                        return true;
                    }
                }

                break;
            case AffectedUnitsType.ENEMY_UNITS:
                if (characterAt != null) {
                    if (characterAt.Player != user.Player) {
                        return true;
                    }
                }

                break;
            case AffectedUnitsType.ALL_UNITS:
                if (characterAt != null) {
                    return true;
                }

                break;
            case AffectedUnitsType.EMPTY_FIELDS:
                if (isTargetFree) {
                    return true;
                }

                break;
            case AffectedUnitsType.ALL_CONSTRUCTS:
                if (!isTargetFree && usedData.GetUnitAt(target).IsSummon) {
                    return true;
                }

                break;

            default: throw new ArgumentOutOfRangeException();
        }

        return false;
    }

}