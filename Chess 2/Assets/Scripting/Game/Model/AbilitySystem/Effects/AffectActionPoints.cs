﻿using System.Collections.Generic;
using System.Linq;
using FullSerializer;
using NUnit.Framework;
using UnityEngine;


[CreateAssetMenu]
public class AffectActionPoints : Effect {

    [SerializeField] private bool SetValue;
    [SerializeField] private bool AddValue;
    [SerializeField] private int Value;

    private Stack<int> oryginalValuesHistory = new Stack<int>();
    private Stack<Character> targetedCharactersHistory = new Stack<Character>();
    private Stack<int> numberOfTargetsHistory = new Stack<int>();

    public override void Init() {
        base.Init();
        oryginalValuesHistory.Clear();
        targetedCharactersHistory.Clear();
        numberOfTargetsHistory.Clear();
    }

    public override void OnCast(List<Vector2> targets, Character caster, BoardData usedData) {

        data = usedData;
        foreach (Vector2 target in targets) {
            Character targetedCharacter = data.GetCharacterAt(target);
            targetedCharactersHistory.Push(targetedCharacter);
            if (targetedCharacter != null) {
                int oryginalValue = targetedCharacter.ActionPoints;
                oryginalValuesHistory.Push(oryginalValue);

                if (AddValue) {
                    targetedCharacter.ActionPoints += Value;
                }
                if (SetValue) {
                    targetedCharacter.ActionPoints = Value;
                }
            }
        }
        numberOfTargetsHistory.Push(targets.Count());

    }

    public override void Undo() {
        int targetsCount = numberOfTargetsHistory.Pop();
        for (int i = 0; i < targetsCount; i++) {
            Character targetedCharacter = targetedCharactersHistory.Pop();
            if (targetedCharacter != null) {
                targetedCharacter.ActionPoints = oryginalValuesHistory.Pop();
            }
        }
    }

}