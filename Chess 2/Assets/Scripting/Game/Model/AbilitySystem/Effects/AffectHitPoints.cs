﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu]
public class AffectHitPoints : Effect {

    [SerializeField] private bool AddValue;
    [SerializeField] private bool SetValue;
    [SerializeField] private int Value;

    private Stack<int> oryginalValuesHistory = new Stack<int>();
    private Stack<Character> targetedCharactersHistory = new Stack<Character>();
    private Stack<int> numberOfTargetsHistory = new Stack<int>();

    public override void Init() {
        base.Init();
        oryginalValuesHistory.Clear();
        targetedCharactersHistory.Clear();
        numberOfTargetsHistory.Clear();
    }

    public override void OnCast(List<Vector2> targets, Character caster, BoardData usedData) {

        

        data = usedData;

        foreach (Vector2 target in targets) {
            Character targetedCharacter = data.GetCharacterAt(target);
            targetedCharactersHistory.Push(targetedCharacter);
            if (targetedCharacter != null) {
                int oryginalValue = targetedCharacter.HitPoints;
                oryginalValuesHistory.Push(oryginalValue);

                if (SetValue) {
                    targetedCharacter.HitPoints = Value;
                }
                if (AddValue) {
                    targetedCharacter.HitPoints += Value;
                    if (targetedCharacter.HitPoints <= 0) {
                        usedData.Graveyard.Add(targetedCharacter);
                        usedData.Units.Remove(targetedCharacter);
                    }
                    if (targetedCharacter.HitPoints > targetedCharacter.Statistics[StatisticType.MaxHitPoints].Value) {
                        targetedCharacter.HitPoints = targetedCharacter.Statistics[StatisticType.MaxHitPoints].Value;
                    }
                }
            }
        }
        numberOfTargetsHistory.Push(targets.Count());
    }

    public override void Undo() {
        int targetsCount = numberOfTargetsHistory.Pop();
        for (int i = 0; i < targetsCount; i++) {
            Character targetedCharacter = targetedCharactersHistory.Pop();
            if (targetedCharacter != null) {
                if (targetedCharacter.HitPoints <= 0) {
                    data.Graveyard.Remove(targetedCharacter);
                    data.Units.Add(targetedCharacter);
                }
                targetedCharacter.HitPoints = oryginalValuesHistory.Pop();
            }
        }
    }

}