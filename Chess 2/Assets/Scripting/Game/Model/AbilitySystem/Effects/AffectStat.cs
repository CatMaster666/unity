﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;


[CreateAssetMenu]
public class AffectStat : Effect {

    public StatisticType StatType;
    [SerializeField] private bool SetAmount;
    [SerializeField] private bool SubtractAmount;
    public int Amount;

    private Stack<int> oryginalValuesHistory = new Stack<int>();
    private Stack<Character> targetedCharactersHistory = new Stack<Character>();
    private Stack<int> numberOfTargetsHistory = new Stack<int>();

    public override void Init() {
        base.Init();
        oryginalValuesHistory.Clear();
        targetedCharactersHistory.Clear();
        numberOfTargetsHistory.Clear();
    }
    
    public override void OnCast(List<Vector2> targets, Character caster, BoardData usedData) {

        data = usedData;

        foreach (Vector2 target in targets) {
            Character targetedCharacter = data.GetCharacterAt(target);
            targetedCharactersHistory.Push(targetedCharacter);
            if (targetedCharacter != null) {
                int oryginalValue = targetedCharacter.Statistics[StatType].Value;
                oryginalValuesHistory.Push(oryginalValue);

                if (SubtractAmount) {
                    targetedCharacter.Statistics[StatType].Value -= Amount;
                }

                if (SetAmount) {
                    targetedCharacter.Statistics[StatType].Value = Amount;
                }

            }

        }
        numberOfTargetsHistory.Push(targets.Count);

    }

    public override void Undo() {
        int targetsCount = numberOfTargetsHistory.Pop();
        for (int i = 0; i < targetsCount; i++) {
            Character targetedCharacter = targetedCharactersHistory.Pop();
            if (targetedCharacter != null) {
                targetedCharacter.Statistics[StatType].Value = oryginalValuesHistory.Pop();
            }
        }
    }

}