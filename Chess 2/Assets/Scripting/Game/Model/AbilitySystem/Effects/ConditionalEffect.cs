﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu]
public class ConditionalEffect : Effect {

    [SerializeField] private List<Condition> conditions;
    [SerializeField] private List<Effect> effectsIfConditionsFalse;
    [SerializeField] private List<Effect> effectsIfConditionsTrue;

    private Stack<bool> testsResultsHistory = new Stack<bool>();
    private Stack<int> numberOfTargetsHistory = new Stack<int>();

    public override void Init() {
        base.Init();
        testsResultsHistory.Clear();
        numberOfTargetsHistory.Clear();
    }
    public override void OnCast(List<Vector2> targets, Character caster, BoardData usedData) {

        data = usedData;

        foreach (Vector2 target in targets) {

            bool testsPassed = true;
            foreach (Condition test in conditions) {
                if (!test.TestPassed(caster, target, data)) {
                    testsPassed = false;
                }
            }
            testsResultsHistory.Push(testsPassed);

            if (testsPassed) {
                foreach (Effect effect in effectsIfConditionsTrue) {
                    effect.OnCast(new List<Vector2> {target}, caster, data);
                }
            }

            else {
                foreach (Effect effect in effectsIfConditionsFalse) {
                    effect.OnCast(new List<Vector2> {target}, caster, data);
                }
            }
        }
        numberOfTargetsHistory.Push(targets.Count());
    }

    public override void Undo() {
        int targetsCount = numberOfTargetsHistory.Pop();
        for (int i = 0; i < targetsCount; i++) {

            if (testsResultsHistory.Pop()) {
                foreach (Effect effect in effectsIfConditionsTrue) {
                    effect.Undo();
                }
            }
            else {
                foreach (Effect effect in effectsIfConditionsFalse) {
                    effect.Undo();
                }
            }
        }
    }

}