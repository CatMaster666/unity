﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu]
public class DisplacementEffect : Effect {

    private Stack<Vector2> oryginalTargetsPositionsHistory = new Stack<Vector2>();
    private Stack<Vector2> oryginalCasterPositionHistory = new Stack<Vector2>();
    private Stack<Character> targetedCharactersHistory = new Stack<Character>();

    public override void Init() {
        base.Init();
        oryginalCasterPositionHistory.Clear();
        targetedCharactersHistory.Clear();
        oryginalTargetsPositionsHistory.Clear();
    }
    
    public override void OnCast(List<Vector2> targets, Character caster, BoardData usedData) {

        data = usedData;
        castersHistory.Push(caster);
        oryginalCasterPositionHistory.Push(caster.Position);

        Vector2 oryginalValue = targets[0];
        Character targetedCharacter = data.GetCharacterAt(oryginalValue);

        if (targetedCharacter != null) {
            targetedCharactersHistory.Push(targetedCharacter);
            oryginalTargetsPositionsHistory.Push(oryginalValue);
            targetedCharacter.Position = caster.Position;
        }

        caster.Position = oryginalValue;
    }

    public override void Undo() {

        Character caster = castersHistory.Count > 0
            ? castersHistory.Pop()
            : null;
        if (caster != null) {

            if (targetedCharactersHistory.Count > 0) {
                Character targetedCharacter = targetedCharactersHistory.Pop();
                if (targetedCharacter != null) {
                    targetedCharacter.Position = oryginalTargetsPositionsHistory.Pop();
                }
            }

            caster.Position = oryginalCasterPositionHistory.Pop();
        }

    }

}