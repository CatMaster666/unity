﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu]
public class DurationEffect : Effect {

    public enum DurationEffectTag {

        Stuned,
        Bleeding,
        Burning,
        Stimulated,
        Slowed,
        Sick,
        MagicProtection,
        Virus

    }


    [SerializeField] private DurationEffectType DurationType;
    public int Duration;
    public DurationEffectTag EffectTag;
    public Effect UsedEffect;
    public Effect DurationEndEffect;

    private Stack<Character> targetedCharactersHistory = new Stack<Character>();
    private Stack<int> numberOfTargetsHistory = new Stack<int>();

    public override void Init() {
        base.Init();        
        targetedCharactersHistory.Clear();
        numberOfTargetsHistory.Clear();
    }
    
    public override void OnCast(List<Vector2> targets, Character caster, BoardData usedData) {

        data = usedData;

        castersHistory.Push(caster);

        foreach (Vector2 target in targets) {
            Character targetedCharacter = data.GetCharacterAt(target);
            targetedCharactersHistory.Push(targetedCharacter);
            if (targetedCharacter != null) {
                if (UsedEffect != null && DurationType != DurationEffectType.EveryTurnForXTurns) {
                    UsedEffect.OnCast(new List<Vector2> {target}, caster, data);
                }

                targetedCharacter.AppliedEffects.Add(this);
            }
        }
        numberOfTargetsHistory.Push(targets.Count);

    }

    public void OnTick(Vector2 target) {
        if (DurationType == DurationEffectType.EveryTurnForXTurns) {
            UsedEffect.OnCast(new List<Vector2> {target}, castersHistory.Peek(), data);
        }
    }

    public void OnDurationEnd(Vector2 target) {
        if (UsedEffect != null) {
            if (DurationType == DurationEffectType.ForXTurns) {
                UsedEffect.Undo();
            }
        }
        if (DurationEndEffect != null) {
            DurationEndEffect.OnCast(new List<Vector2> {target}, castersHistory.Peek(), data);
        }

        //DestroyImmediate(this);
    }

    public override void Undo() {
        int targetsCount = numberOfTargetsHistory.Pop();
        for (int i = 0; i < targetsCount; i++) {
            Character targetedCharacter = targetedCharactersHistory.Pop();
            if (targetedCharacter != null) {
                if (UsedEffect != null && DurationType != DurationEffectType.EveryTurnForXTurns) {
                    UsedEffect.Undo();
                }

                targetedCharacter.AppliedEffects.Remove(this);
            }
        }
    }


    private enum DurationEffectType {

        ForXTurns,
        EveryTurnForXTurns

    }

}