﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu]
public class RemoveStatusEffect : Effect {

    [SerializeField] private bool RemoveAllInstances;
    [SerializeField] private DurationEffect.DurationEffectTag EffectTag;

    private Stack<List<DurationEffect>> removedEffectsHistory = new Stack<List<DurationEffect>>();
    private Stack<Character> targetedCharactersHistory = new Stack<Character>();
    private Stack<int> numberOfTargetsHistory = new Stack<int>();

    public override void Init() {
        base.Init();
        removedEffectsHistory.Clear();
        targetedCharactersHistory.Clear();
        numberOfTargetsHistory.Clear();
    }
    
    public override void OnCast(List<Vector2> targets, Character caster, BoardData usedData) {

        data = usedData;
        castersHistory.Push(caster);

        foreach (Vector2 target in targets) {
            Character targetedCharacter = data.GetCharacterAt(target);
            targetedCharactersHistory.Push(targetedCharacter);
            if (targetedCharacter != null) {
                List<DurationEffect> removedEffects = new List<DurationEffect>();
                if (RemoveAllInstances) {
                    foreach (DurationEffect effect in targetedCharacter.AppliedEffects.FindAll(e => e.EffectTag == EffectTag)) {
                        removedEffects.Add(effect);
                        effect.Undo();
                    }
                    targetedCharacter.AppliedEffects.RemoveAll(e => e.EffectTag == EffectTag);
                }
                else {
                    if (targetedCharacter.AppliedEffects.FirstOrDefault(e => e.EffectTag == EffectTag) != null) {
                        targetedCharacter.AppliedEffects.FirstOrDefault(e => e.EffectTag == EffectTag).Undo();
                        removedEffects.Add(targetedCharacter.AppliedEffects.FirstOrDefault(e => e.EffectTag == EffectTag));
                        targetedCharacter.AppliedEffects.Remove(targetedCharacter.AppliedEffects.FirstOrDefault(e => e.EffectTag == EffectTag));
                    }
                }
                removedEffectsHistory.Push(removedEffects);
            }
        }

        numberOfTargetsHistory.Push(targets.Count());
    }

    public override void Undo() {
        int targetsCount = numberOfTargetsHistory.Pop();
        for (int i = 0; i < targetsCount; i++) {
            Character targetedCharacter = targetedCharactersHistory.Pop();
            if (targetedCharacter != null) {
                foreach (DurationEffect effect in removedEffectsHistory.Pop()) {
                    effect.OnCast(new List<Vector2> {targetedCharacter.Position}, effect.castersHistory.Peek(), data);
                }
            }
        }
    }

}