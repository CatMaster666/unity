﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu]
public class SummonBasicObstacle : BaseSummon {

    private Stack<Unit> summonedUnitsHistory = new Stack<Unit>();
    private Stack<int> numberOfTargetsHistory = new Stack<int>();

    public override void Init() {
        base.Init();
        summonedUnitsHistory.Clear();
        numberOfTargetsHistory.Clear();
    }
    
    public override void OnCast(List<Vector2> targets, Character caster, BoardData usedData) {
        data = usedData;

        foreach (Vector2 target in targets) {

            Unit U = new Unit();
            U.InitUnit((int) target.x, (int) target.y, "CrateSprite", true, Duration);
            summonedUnitsHistory.Push(U);
            data.Units.Add(U);
        }
        numberOfTargetsHistory.Push(targets.Count());
    }

    public override void Undo() {

        int targetsCount = numberOfTargetsHistory.Pop();
        for (int i = 0; i < targetsCount; i++) {

            data.Units.Remove(summonedUnitsHistory.Pop());
        }
    }

}