﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu]
public class SummonWarrior : BaseSummon {

    public Ability Basic;
    public int HP;
    public int MagicResistance;
    public int MovementRange;

    private Stack<Character> summonedCharactersHistory = new Stack<Character>();
    private Stack<int> numberOfTargetsHistory = new Stack<int>();

    public override void Init() {
        base.Init();
        summonedCharactersHistory.Clear();
        numberOfTargetsHistory.Clear();
    }

    public override void OnCast(List<Vector2> targets, Character caster, BoardData usedData) {
        data = usedData;

        foreach (Vector2 target in targets) {
            Character summon = new Character(CharacterClass.Warrior, usedData.Turn);

            summon.Statistics[StatisticType.MaxHitPoints].Value = HP;
            summon.Statistics[StatisticType.MovementRange].Value = MovementRange;
            summon.Statistics[StatisticType.MagicResistance].Value = MagicResistance;
            summon.Position = target;
            summon.IsSummon = true;
            summon.Duration = Duration;
            summon.ActionPoints = 0;
            summon.BasicAttack = Instantiate(Basic);

            summonedCharactersHistory.Push(summon);
            data.Units.Add(summon);
        }
        numberOfTargetsHistory.Push(targets.Count());

    }

    public override void Undo() {
        int targetsCount = numberOfTargetsHistory.Pop();
        for (int i = 0; i < targetsCount; i++) {

            data.Units.Remove(summonedCharactersHistory.Pop());
        }
    }

}