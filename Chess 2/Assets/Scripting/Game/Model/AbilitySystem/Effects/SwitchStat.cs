﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu]
public class SwitchStat : Effect {

    [SerializeField] private StatisticType StatType;

    private Stack<Character> targetedCharactersHistory = new Stack<Character>();

    public override void Init() {
        base.Init();
        targetedCharactersHistory.Clear();
    }

    public override void OnCast(List<Vector2> targets, Character caster, BoardData usedData) {

        data = usedData;
        Character targetedCharacter = data.GetCharacterAt(targets.First());
        targetedCharactersHistory.Push(targetedCharacter);
        castersHistory.Push(caster);
        if (targetedCharacter != null || caster == null) {

            int targetStatValue = targetedCharacter.Statistics[StatType].Value;
            int casterStatValue = caster.Statistics[StatType].Value;

            targetedCharacter.Statistics[StatType].Value = casterStatValue;
            caster.Statistics[StatType].Value = targetStatValue;
        }

    }

    public override void Undo() {

        Character caster = castersHistory.Pop();
        Character target = targetedCharactersHistory.Pop();

        if (target != null || caster == null) {

            int targetStatValue = target.Statistics[StatType].Value;
            int casterStatValue = caster.Statistics[StatType].Value;

            target.Statistics[StatType].Value = casterStatValue;
            caster.Statistics[StatType].Value = targetStatValue;
        }
    }

}