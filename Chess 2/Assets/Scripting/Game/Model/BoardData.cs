﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Random = UnityEngine.Random;


public enum State {

    SelectingUnit,
    Movement,
    Attack,
    Cast,
    EndOfTurn,
    SelectingAbility

}


[Serializable]
public class BoardData {

    public const int Width = 10;
    public const int Height = 10;
    public State GameState = State.SelectingUnit;
    public List<Unit> Units = new List<Unit>();
    public List<Character> Graveyard = new List<Character>();
    public bool Turn = true;

    public Character GetSelecetedUnit() {
        for (int index = 0; index < Units.Count; index++) {
            if (Units[index] is Character) {
                if ((Units[index] as Character).Selected) {
                    return Units[index] as Character;
                }
            }
        }

        return null;
    }

    public Unit GetUnitAt(Vector2 position) {
        for (int index = 0; index < Units.Count; index++) {
            Unit u = Units[index];
            if (u.Position == position) {
                return u;
            }
        }

        return null;
    }

    public Character GetCharacterAt(Vector2 position) {

        for (int index = 0; index < Units.Count; index++) {
            Unit c = Units[index];
            if (c.Position == position && c is Character) {
                return c as Character;
            }
        }

        return null;
    }

    public List<Vector2> GetNeigbours(Character character) {
        List<Vector2> buffer = new List<Vector2>();
        if (IsFree((int) character.Position.x - 1, (int) character.Position.y)) {
            buffer.Add(new Vector2(character.Position.x - 1, character.Position.y));
        }
        if (IsFree((int) character.Position.x - 1, (int) character.Position.y - 1)) {
            buffer.Add(new Vector2(character.Position.x - 1, character.Position.y - 1));
        }
        if (IsFree((int) character.Position.x - 1, (int) character.Position.y + 1)) {
            buffer.Add(new Vector2(character.Position.x - 1, character.Position.y));
        }
        if (IsFree((int) character.Position.x + 1, (int) character.Position.y + 1)) {
            buffer.Add(new Vector2(character.Position.x + 1, character.Position.y + 1));
        }
        if (IsFree((int) character.Position.x + 1, (int) character.Position.y)) {
            buffer.Add(new Vector2(character.Position.x + 1, character.Position.y));
        }
        if (IsFree((int) character.Position.x + 1, (int) character.Position.y - 1)) {
            buffer.Add(new Vector2(character.Position.x + 1, character.Position.y - 1));
        }
        if (IsFree((int) character.Position.x, (int) character.Position.y - 1)) {
            buffer.Add(new Vector2(character.Position.x + 1, character.Position.y - 1));
        }
        if (IsFree((int) character.Position.x, (int) character.Position.y + 1)) {
            buffer.Add(new Vector2(character.Position.x + 1, character.Position.y - 1));
        }
        return buffer;
    }

    public bool IsFree(int x, int y) {
        for (int index = 0; index < Units.Count; index++) {
            if (Units[index].Position == new Vector2(x, y)) {
                return false;
            }
        }

        return x < Width && x >= 0 && y < Height && y >= 0;
    }

    public bool IsNotFree(Vector2 position) {
        for (int index = 0; index < Units.Count; index++) {
            if (Units[index].Position == position) {
                return true;
            }
        }

        return false;

    }

    public void SelectCharacter(Character C) {
        if (C.AppliedEffects.Find(x => x.EffectTag == DurationEffect.DurationEffectTag.Stuned)) {
            return;
        }

        if (GetSelecetedUnit() != null) {
            GetSelecetedUnit().Selected = false;
        }

        C.Selected = true;

    }

    public void DeselectUnits() {
        foreach (Unit unit in Units) {
            Character selectedCharacter = unit as Character;
            if (selectedCharacter != null) {
                if (selectedCharacter.Selected) {
                    selectedCharacter.Selected = false;
                }
            }
        }
    }

    public static Vector2 MouseToBoardPosition() {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (!Physics.Raycast(ray, out hit, 10000.0f)) {
            return new Vector2(1000, 1000);
        }

        if (hit.transform.GetComponentInChildren<Field>() != null) {
            return hit.transform.GetComponentInChildren<Field>().Position;
        }

        return new Vector2(1000, 1000);
    }

    public static Vector2 GetRandomFreePosition() {
        Vector2 position = new Vector2(Random.Range(0, Width), Random.Range(0, Height));

        while (ResourcesManager.Instance.Data.Units.Exists(x => x.Position == position)) {
            position = new Vector2(Random.Range(0, BoardData.Width), Random.Range(0, BoardData.Height));
        }

        return position;
    }

    public void Save() {
        string save = JsonSerializer.Serialize(typeof(BoardData), ResourcesManager.Instance.Data);
        Debug.Log(save);
        File.WriteAllText(Application.persistentDataPath + "save", save);
    }

    public void Load() {
        string save = File.ReadAllText(Application.persistentDataPath + "save");
        ResourcesManager.Instance.Data = (BoardData) JsonSerializer.Deserialize(typeof(BoardData), save);
    }

}