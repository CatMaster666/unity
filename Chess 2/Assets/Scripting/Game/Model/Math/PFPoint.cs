﻿public class PfPoint {

    public readonly int X;
    public readonly int Y;

    public PfPoint Parent;
    public bool Visited;

    public PfPoint(int x, int y, PfPoint parent) {
        X = x;
        Y = y;
        Parent = parent;
        Visited = false;
    }

}