﻿using System.Collections.Generic;
using UnityEngine;


public class PathfindingModel {

    //layermask used to ignore everything other then "fields" layer while casting visibiliti rays

    //grid used only for pathfinding
    private PfPoint[,] grid;
    private BoardData data;
/////////////////////////////////////////INITIALIZATION///////////////////////////////////////////////

    public PathfindingModel(BoardData boardData) {
        data = boardData;
        grid = new PfPoint[BoardData.Width, BoardData.Height];
        for (int x = 0; x < BoardData.Width; x++) {
            for (int y = 0; y < BoardData.Height; y++) {
                grid[x, y] = new PfPoint(x, y, null);
            }
        }
    }

////////////////////////////////////////PATHFINDING///////////////////////////////////////////////////

    public void CalculatePathfindingGraph(Vector2 start) {

        for (int x = 0; x < BoardData.Width; x++) {
            for (int y = 0; y < BoardData.Height; y++) {
                grid[x, y].Visited = false;
                grid[x, y].Parent = null;
            }
        }

        Queue<PfPoint> q = new Queue<PfPoint>();
        q.Enqueue(new PfPoint((int) start.x,
                              (int) start.y, null));

        while (q.Count != 0) {

            PfPoint p = q.Dequeue();
            p.Visited = true;

            if (data.IsFree(p.X - 1, p.Y) && !grid[p.X - 1, p.Y].Visited) {
                grid[p.X - 1, p.Y].Visited = true;
                grid[p.X - 1, p.Y].Parent = p;
                PfPoint nextP = new PfPoint(p.X - 1, p.Y, p);
                q.Enqueue(nextP);
            }
            if (data.IsFree(p.X + 1, p.Y) && !grid[p.X + 1, p.Y].Visited) {
                grid[p.X + 1, p.Y].Visited = true;
                grid[p.X + 1, p.Y].Parent = p;
                PfPoint nextP = new PfPoint(p.X + 1, p.Y, p);
                q.Enqueue(nextP);
            }
            if (data.IsFree(p.X, p.Y + 1) && !grid[p.X, p.Y + 1].Visited) {
                grid[p.X, p.Y + 1].Visited = true;
                grid[p.X, p.Y + 1].Parent = p;
                PfPoint nextP = new PfPoint(p.X, p.Y + 1, p);
                q.Enqueue(nextP);
            }
            if (data.IsFree(p.X, p.Y - 1) && !grid[p.X, p.Y - 1].Visited) {
                grid[p.X, p.Y - 1].Visited = true;
                grid[p.X, p.Y - 1].Parent = p;
                PfPoint nextP = new PfPoint(p.X, p.Y - 1, p);
                q.Enqueue(nextP);
            }
        }
    }

    public List<Vector2> GetPath(Vector2 destination, int range) {

        List<Vector2> path = new List<Vector2>();

        foreach (PfPoint p in grid) {
            if (p.X == destination.x && p.Y == destination.y && p.Visited) {
                PfPoint P = p;
                int i = 0;

                while (P.Parent != null) {
                    path.Add(new Vector2(P.X, P.Y));
                    P = P.Parent;
                    i++;

                    if (i > range) {
                        path.Clear();
                        return path;
                    }
                }
                return path;
            }
        }
        return path;
    }

    public List<Vector2> GetValidPaths(int range) {
        List<Vector2> validPathsBuffer = new List<Vector2>();
        for (int x = 0; x < BoardData.Width; x++) {
            for (int y = 0; y < BoardData.Height; y++) {
                validPathsBuffer.AddRange(GetPath(new Vector2(x, y), range));
            }
        }
        return validPathsBuffer;
    }

    public bool IsReachable(Vector2 destination) {
        foreach (PfPoint p in grid) {
            if (p.X == destination.x && p.Y == destination.y) {
                return p.Visited;
            }
        }
        return false;
    }

}