﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;


public class VisibilityRaycaster {

    private static BoardView view;
    private static Canvas canvas;

    private const int layerMask = 1 << 8;

    public static void Init() {
        view = Object.FindObjectOfType<BoardView>();
        canvas = Object.FindObjectOfType<Canvas>();
    }

    public static List<Vector2> GetVisibleTargets(Vector2 startingPoint, int Range, BoardData usedData) {
        List<Vector2> buffer = new List<Vector2>();
        float canvasScale = canvas.transform.lossyScale.x;
        float range = Range * view.FieldSize.x * canvasScale; //+ 0.49f * view.FieldSize.x * canvasScale;
        int raysToShoot = 90;

        float angle = 0;
        Vector2 startingBoardPosition = BoardToWorldPosition(startingPoint) + view.FieldSize / 2 * canvasScale;
        for (int i = 0; i < raysToShoot; i++) {
            float x = Mathf.Sin(angle);
            float y = Mathf.Cos(angle);
            angle += 2 * Mathf.PI / raysToShoot;

            Vector2 dir = new Vector3(x, y);
            buffer.AddRange(VisibilityRay(dir, startingPoint, startingBoardPosition, range, usedData));
        }
        return buffer;
    }

    private static List<Vector2> VisibilityRay(Vector3 direction, Vector2 startBoard, Vector2 startWorld, float range, BoardData usedData) {
        List<Vector2> buffer = new List<Vector2>();
        RaycastHit2D[] hits = Physics2D.RaycastAll(startWorld, direction, range, layerMask);

        Debug.DrawRay(startWorld, direction * range, Color.red, 2);
        for (int index = 0; index < hits.Length; index++) {

            Vector2 HitBoardPosition = hits[index].transform.parent.GetComponentInParent<Field>().Position;
            if (usedData.IsNotFree(HitBoardPosition)) {
                if (HitBoardPosition == startBoard) {
                    buffer.Add(HitBoardPosition);
                }
                else {
                    buffer.Add(HitBoardPosition);
                    return buffer;
                }
            }
            else {
                buffer.Add(HitBoardPosition);

            }
        }
        return buffer;
    }

    private static Vector2 BoardToWorldPosition(Vector2 b) {
        for (int x = 0; x < BoardData.Width; x++) {
            for (int y = 0; y < BoardData.Height; y++) {
                if (view.Fields[x, y].Position == b) {
                    return view.Fields[x, y].transform.position;
                }
            }
        }
        return new Vector2();
    }

}