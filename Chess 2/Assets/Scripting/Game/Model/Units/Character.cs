﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;


public enum CharacterClass {

    Mage,
    Warrior,
    Engineer,
    Medic

}


[Serializable]
public class Character : Unit {

    [SerializeField]
    private CharacterClass Class;

    [SerializeField]
    public StatsDictionary Statistics;
    public int HitPoints;
    public int ActionPoints;
    public bool Player;
    public bool Selected;

    public List<DurationEffect> AppliedEffects = new List<DurationEffect>();
    public List<Ability> AvailibleAbilities = new List<Ability>();
    public Ability BasicAttack;
    public Ability SelectedAbility;

    public Character() {
        builder = new StringFast();
    }

    public Character(CharacterClass job, bool player) {
        Class = job;
        ClassTemplates template = ResourcesManager.Instance.classes;
        switch (Class) {
            case CharacterClass.Mage:
                Statistics = Object.Instantiate(template.mage).Stats;
                AvailibleAbilities = Object.Instantiate(template.mage).Skills;
                BasicAttack = Object.Instantiate(template.mage).Basic;

                break;
            case CharacterClass.Engineer:
                Statistics = Object.Instantiate(template.engineer).Stats;
                AvailibleAbilities = Object.Instantiate(template.engineer).Skills;
                BasicAttack = Object.Instantiate(template.engineer).Basic;

                break;
            case CharacterClass.Warrior:
                Statistics = Object.Instantiate(template.warrior).Stats;
                AvailibleAbilities = Object.Instantiate(template.warrior).Skills;
                BasicAttack = Object.Instantiate(template.warrior).Basic;

                break;
            case CharacterClass.Medic:
                Statistics = Object.Instantiate(template.medic).Stats;
                AvailibleAbilities = Object.Instantiate(template.medic).Skills;
                BasicAttack = Object.Instantiate(template.medic).Basic;

                break;
        }

        SpriteName = job + "Sprite";
        Player = player;
        ActionPoints = 1;
        HitPoints = Statistics[StatisticType.MaxHitPoints].Value;
    }

    public Ability GetAbilityInstance(Ability ability) {
        return Object.Instantiate(ability);

    }

   

    public override void OnTurnEnd() {
        base.OnTurnEnd();
        foreach (DurationEffect effect in AppliedEffects) {
            effect.OnTick(Position);
        }

        foreach (DurationEffect effect in AppliedEffects) {
            effect.Duration--;
        }

        foreach (DurationEffect effect in AppliedEffects.FindAll(e => e.Duration <= 0)) {
            effect.OnDurationEnd(Position);

        }

        AppliedEffects.RemoveAll(e => e.Duration <= 0);

        if (Player != ResourcesManager.Instance.Data.Turn) {
            ActionPoints += Statistics[StatisticType.ActionPointsPerTurn].Value;
            Statistics[StatisticType.ActionPointsPerTurn].Value++;
        }

    }

    public string GetFullDescryption() {
        ToString();
        builder.Append(Class.ToString()).Append("\nActionPoints : ").Append(ActionPoints);
        builder.Append("\nHP : ").Append(HitPoints).Append("/").Append(Statistics[StatisticType.MaxHitPoints].Value);
        builder.Append("\n\nStatistics : \n");

        foreach (SingleStatistic singleStatistic in Statistics.Values) {
            if (singleStatistic.StatType != StatisticType.MaxHitPoints) {
                builder.Append(singleStatistic.StatType).Append(" : ").Append(+singleStatistic.Value).Append("\n");
            }
        }
        builder.Append("\n\nStatus effects : \n");

        foreach (DurationEffect t1 in AppliedEffects) {
            builder.Append(t1.EffectTag).Append(" duration(").Append(t1.Duration).Append(")\n");
        }
        return builder.ToString();
    }

}