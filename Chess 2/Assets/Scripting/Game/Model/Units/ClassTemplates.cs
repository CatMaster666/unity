﻿using System;


[Serializable]
public class ClassTemplates {

    public ClassIdentity engineer;
    public ClassIdentity mage;
    public ClassIdentity medic;
    public ClassIdentity warrior;

    public void Init() {
        engineer.Init();
        mage.Init();
        medic.Init();
        warrior.Init();
    }
}