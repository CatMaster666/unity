﻿using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
[CreateAssetMenu]
public class ClassIdentity : ScriptableObject {

    [SerializeField] public StatsDictionary Stats = new StatsDictionary();
    [SerializeField] public List<Ability> Skills = new List<Ability>();
    [SerializeField] public Ability Basic;

    public void Init() {
        foreach (Effect effect in Basic.TargetedEffects) {
            effect.Init();
        }
        foreach (Ability ability in Skills) {
            foreach (Effect effect in ability.TargetedEffects) {
                effect.Init();
            }
            foreach (Effect effect in ability.SelfTargetingEffects) {
                effect.Init();
            }
        }
    }

}