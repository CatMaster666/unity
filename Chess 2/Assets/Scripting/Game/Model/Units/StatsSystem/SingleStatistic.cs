﻿using System;


[Serializable]
public enum StatisticType {

    ActionPointsPerTurn,
    MaxHitPoints,
    MagicResistance,
    MovementRange

}


[Serializable]
public class SingleStatistic {

    public StatisticType StatType;
    public int Value;

}