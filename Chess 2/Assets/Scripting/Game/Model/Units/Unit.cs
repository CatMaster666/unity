﻿using System;
using UnityEngine;


[Serializable]
public class Unit {

    protected StringFast builder;
    public int Duration;
    public bool IsSummon;
    public Vector2 Position;
    public string SpriteName;

    public Unit() {
        builder = new StringFast();
    }

    public void InitUnit(int x, int y, string s, bool isSummon, int duration) {
        Position.x = x;
        Position.y = y;
        SpriteName = s;
        IsSummon = isSummon;
        Duration = duration;
    }

    public override string ToString() {
        builder.Clear();
        if (IsSummon) {
            builder.Append("Summon").Append(" (dies in ").Append(Duration).Append(") \n");
        }

        return builder.ToString();
    }

    public virtual void OnTurnEnd() {
        if (IsSummon) {
            Duration--;
        }
    }

}