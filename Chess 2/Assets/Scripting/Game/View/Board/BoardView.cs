﻿using UnityEngine;
using UnityEngine.UI;


public class BoardView : MonoBehaviour {

    [SerializeField] private GameObject Board;
    [SerializeField] private GameObject fieldPrefab;

    //used by PathfindingModel
    [HideInInspector] public Field[,] Fields;
    [HideInInspector] public Vector2 FieldSize;

    //helper is creating ColorBuffers lists used by BoardView to draw the board
    private ViewHelper helper;

    private void Start() {

        Fields = new Field[BoardData.Width, BoardData.Height];
        FieldSize = Vector2.one * (Board.GetComponent<RectTransform>().rect.height - 10) / BoardData.Height;
        Board.GetComponent<GridLayoutGroup>().cellSize = FieldSize;
        VisibilityRaycaster.Init();
        for (int x = 0; x < BoardData.Width; x++) {
            for (int y = 0; y < BoardData.Height; y++) {

                GameObject field = Instantiate(fieldPrefab, Board.transform);
                BoxCollider2D visibilityCollider = field.GetComponentInChildren<BoxCollider2D>();
                BoxCollider mouseInputCollider = field.GetComponent<BoxCollider>();

                visibilityCollider.size = FieldSize + new Vector2(0.001f, 0.001f);
                visibilityCollider.offset = FieldSize / 2;
                mouseInputCollider.size = FieldSize + new Vector2(0.001f, 0.001f);
                mouseInputCollider.center = FieldSize / 2;

                Fields[x, y] = field.GetComponent<Field>();
                Fields[x, y].Position = new Vector2(x, y);
                Fields[x, y].Init();
            }
        }
    }

    public void Init(ViewHelper viewHelper) {
        helper = viewHelper;
    }

    //tags are used for raycasting in PathfindingModel
    public void UpdateTags() {
        for (int x = 0; x < BoardData.Width; x++) {
            for (int y = 0; y < BoardData.Height; y++) {
                Fields[x, y].transform.GetChild(0).tag = "Empty";
            }
        }
        foreach (Unit unit in ResourcesManager.Instance.Data.Units) {
            Fields[(int) unit.Position.x, (int) unit.Position.y].transform.GetChild(0).tag = "Unit";
        }
    }

    public void UpdateFieldsActionPoints() {
        for (int x = 0; x < BoardData.Width; x++) {
            for (int y = 0; y < BoardData.Height; y++) {
                Fields[x, y].UpdateActionPoints();
            }
        }
    }

    public void UpdateFieldsHitPointsBars() {
        for (int x = 0; x < BoardData.Width; x++) {
            for (int y = 0; y < BoardData.Height; y++) {
                Fields[x, y].UpdateHitPointSlider();
            }
        }
    }

    public void UpdateFieldsCursor(Vector2 mousePosition) {
        for (int x = 0; x < BoardData.Width; x++) {
            for (int y = 0; y < BoardData.Height; y++) {
                Fields[x, y].UpdateCursor(mousePosition);
            }
        }
    }

    public void UpdateColors() {
        helper.CalculateBlueGreenColors();

        Board.GetComponent<Image>().color = ResourcesManager.Instance.Data.Turn
            ? ResourcesManager.Instance.ColorPalette["blue"]
            : ResourcesManager.Instance.ColorPalette["green"];

        for (int x = 0; x < BoardData.Width; x++) {
            for (int y = 0; y < BoardData.Height; y++) {
                Fields[x, y].SetUnitImageColor(new Color(0, 0, 0, 0));
                Fields[x, y].SetBackgroundColor(Color.white);
            }
        }

        foreach (Unit u in ResourcesManager.Instance.Data.Units) {
            Fields[(int) u.Position.x, (int) u.Position.y].SetUnitImage(ResourcesManager.Instance.Sprites[u.SpriteName]);
            Fields[(int) u.Position.x, (int) u.Position.y].SetUnitImageColor(new Color(1, 1, 1, 1));
        }

        if (helper.GreyFields != null) {
            foreach (Vector2 p in helper.GreyFields) {
                Fields[(int) p.x, (int) p.y].SetBackgroundColor(ResourcesManager.Instance.ColorPalette["gray"]);
            }
        }

        if (helper.GreenFields != null) {
            foreach (Vector2 p in helper.GreenFields) {
                Fields[(int) p.x, (int) p.y].SetBackgroundColor(ResourcesManager.Instance.ColorPalette["green"]);
            }
        }

        if (helper.BlueFields != null) {
            foreach (Vector2 p in helper.BlueFields) {
                Fields[(int) p.x, (int) p.y].SetBackgroundColor(ResourcesManager.Instance.ColorPalette["blue"]);
            }
        }

        if (helper.YellowFields != null) {
            foreach (Vector2 p in helper.YellowFields) {
                Fields[(int) p.x, (int) p.y].SetBackgroundColor(ResourcesManager.Instance.ColorPalette["darkGray"]);
            }
        }

        Character selected = ResourcesManager.Instance.Data.GetSelecetedUnit();
        if (selected != null) {
            Fields[(int) selected.Position.x, (int) selected.Position.y].SetBackgroundColor(ResourcesManager.Instance.ColorPalette["pink"]);
        }

        if (helper.RedFields != null) {
            foreach (Vector2 p in helper.RedFields) {
                Fields[(int) p.x, (int) p.y].SetBackgroundColor(ResourcesManager.Instance.ColorPalette["red"]);
            }
        }

    }

}