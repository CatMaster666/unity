﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class Field : MonoBehaviour {

    private Image backgroundImage;
    private Slider hpSlider;
    public Vector2 Position;
    private TextMeshProUGUI ActionPointsText;
    private Image unitImage;
    private Image selectorImage;

    public void Init() {
        hpSlider = GetComponentInChildren<Slider>();
        backgroundImage = GetComponent<Image>();
        unitImage = GetComponentsInChildren<Image>()[1];
        ActionPointsText = GetComponentInChildren<TextMeshProUGUI>();
        selectorImage = GetComponentsInChildren<Image>()[2];
        selectorImage.gameObject.SetActive(false);
        int random = Random.Range(0, 4);

        switch (random) {
            case 0:
                backgroundImage.sprite = ResourcesManager.Instance.Floor1Sprite;
                break;
            case 1:
                backgroundImage.sprite = ResourcesManager.Instance.Floor2Sprite;
                break;
            case 2:
                backgroundImage.sprite = ResourcesManager.Instance.Floor3Sprite;
                break;
            case 3:
                backgroundImage.sprite = ResourcesManager.Instance.Floor4Sprite;
                break;
        }

        UpdateActionPoints();

    }

///////////////////////METHODS USED BY BOARDVIEW TO UPDATE LOOK OF THE BOARD///////////////////////////////

    public void UpdateActionPoints() {

        Character character = ResourcesManager.Instance.Data.GetUnitAt(Position) as Character;

        ActionPointsText.text = character != null
            ? character.ActionPoints.ToString()
            : string.Empty;
    }

    public void SetUnitImageColor(Color color) {
        unitImage.color = color;
    }

    public void SetUnitImage(Sprite sprite) {
        unitImage.sprite = sprite;
    }

    public void SetBackgroundColor(Color color) {
        backgroundImage.color = color;
    }

    public void UpdateHitPointSlider() {

        Character character = ResourcesManager.Instance.Data.GetUnitAt(Position) as Character;
        if (character != null) {
            hpSlider.gameObject.SetActive(true);
            hpSlider.maxValue = character.Statistics[StatisticType.MaxHitPoints].Value;
            hpSlider.value = character.HitPoints;
        }
        else {
            hpSlider.gameObject.SetActive(false);
        }
    }

    public void UpdateCursor(Vector2 mousePosition) {
        //Character character = ResourcesManager.Instance.Data.GetUnitAt(Position) as Character;
        if (mousePosition == Position) {

            selectorImage.gameObject.SetActive(true);
        }
        else {

            selectorImage.gameObject.SetActive(false);
        }
    }

}