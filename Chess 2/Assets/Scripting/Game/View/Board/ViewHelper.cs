﻿using System.Collections.Generic;
using UnityEngine;


public class ViewHelper {

    public List<Vector2> BlueFields = new List<Vector2>();
    public List<Vector2> GreenFields = new List<Vector2>();
    public List<Vector2> GreyFields = new List<Vector2>();
    public List<Vector2> RedFields = new List<Vector2>();
    public List<Vector2> YellowFields = new List<Vector2>();

    private PathfindingModel pathfinder;

    public ViewHelper(PathfindingModel navigator) {
        pathfinder = navigator;
    }

    public void CalculateBlueGreenColors() {
        BlueFields.Clear();
        GreenFields.Clear();
        foreach (Unit U in ResourcesManager.Instance.Data.Units) {
            if (U is Character) {
                Character u = U as Character;
                if (u.Player == false) {
                    GreenFields.Add(new Vector2(u.Position.x, u.Position.y));
                }
                else {
                    BlueFields.Add(new Vector2(u.Position.x, u.Position.y));
                }
            }
        }
    }

    public void CalculateGrayMovement() {
        pathfinder.CalculatePathfindingGraph(ResourcesManager.Instance.Data.GetSelecetedUnit().Position);
        GreyFields = pathfinder.GetValidPaths(ResourcesManager.Instance.Data.GetSelecetedUnit().Statistics[StatisticType.MovementRange].Value);
    }

    public void CalculateYellowMovement(Vector2 mousePosition) {
        if (mousePosition == new Vector2(1000, 1000)) {
            YellowFields.Clear();
            return;
        }

        YellowFields = pathfinder.GetPath(mousePosition, ResourcesManager.Instance.Data.GetSelecetedUnit().Statistics[StatisticType.MovementRange].Value);
    }

    public void CalculateGrayAbility(Ability skill, Character user) {
        GreyFields = skill.TargetingComponent.GetGrayBuffer(ResourcesManager.Instance.Data, user);
    }

    public void CalculateRedAbility(Ability skill, Character user, Vector2 mousePosition) {
        RedFields.Clear();
        if (mousePosition == new Vector2(1000, 1000)) {
            return;
        }

        if (GreyFields.Contains(mousePosition)) {
            if (ResourcesManager.Instance.Data.GetUnitAt(mousePosition) == null || skill.TargetingComponent.IsTargetValid(mousePosition, user, false, ResourcesManager.Instance.Data)) {
                RedFields.Add(mousePosition);
            }
        }

        if (skill.TargetingComponent.AreaOfEffectTypeRules == AreaOfEffectType.AOE) {
            if (RedFields.Contains(mousePosition)) {
                RedFields.AddRange(skill.TargetingComponent.GetRedBuffer(mousePosition, user, ResourcesManager.Instance.Data));
            }
        }
    }

}