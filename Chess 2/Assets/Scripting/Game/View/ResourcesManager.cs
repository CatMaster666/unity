﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;


[CreateAssetMenu]
public class ResourcesManager : ScriptableObject {

    public static ResourcesManager Instance {
        get {
            if (instance != null) {
                return instance;
            }
            instance = Resources.Load<ResourcesManager>("Data/Resources_Manager");
            instance.Init();
            return instance;
        }

    }
    private static ResourcesManager instance;
    public BoardData Data;
    public ClassTemplates classes;

    //Sprites
    [SerializeField] private Sprite MageSprite;
    [SerializeField] private Sprite EngineerSprite;
    [SerializeField] private Sprite WarriorSprite;
    [SerializeField] private Sprite MedicSprite;
    public Sprite Floor1Sprite;
    public Sprite Floor2Sprite;
    public Sprite Floor3Sprite;
    public Sprite Floor4Sprite;
    [SerializeField] private Sprite CrateSprite;
    [SerializeField] private Sprite MechWarriorSprite;
    public Dictionary<string, Sprite> Sprites = new Dictionary<string, Sprite>();

    //Colors
    [SerializeField] private Color blue;
    [SerializeField] private Color pink;
    [SerializeField] private Color red;
    [SerializeField] private Color darkGray;
    [SerializeField] private Color gray;
    [SerializeField] private Color green;
    public Dictionary<string, Color> ColorPalette = new Dictionary<string, Color>();

    public void Init() {

        classes.Init();

        Sprites.Clear();
        ColorPalette.Clear();

        Sprites.Add("MageSprite", MageSprite);
        Sprites.Add("EngineerSprite", EngineerSprite);
        Sprites.Add("WarriorSprite", WarriorSprite);
        Sprites.Add("MedicSprite", MedicSprite);
        Sprites.Add("CrateSprite", CrateSprite);
        Sprites.Add("MechWarriorSprite", MechWarriorSprite);

        ColorPalette.Add("blue", blue);
        ColorPalette.Add("green", green);
        ColorPalette.Add("red", red);
        ColorPalette.Add("pink", pink);
        ColorPalette.Add("darkGray", darkGray);
        ColorPalette.Add("gray", gray);
    }

}