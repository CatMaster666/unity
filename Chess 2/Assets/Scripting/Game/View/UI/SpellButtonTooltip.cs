﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class SpellButtonTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    private TextMeshProUGUI Descryption;
    private TextMeshProUGUI buttonText;

    private void Start() {
        Descryption = GameObject.Find("Descryption").GetComponent<TextMeshProUGUI>();
        buttonText = GetComponentInChildren<TextMeshProUGUI>();
    }

    public void OnPointerEnter(PointerEventData eventData) {

        Character SelectedCharacter = ResourcesManager.Instance.Data.GetSelecetedUnit();
        if (SelectedCharacter.AvailibleAbilities.Find(x => x.Name == buttonText.text) != null) {
            Descryption.text = TextUtility.ColorText(SelectedCharacter.AvailibleAbilities.Find(x => x.Name == buttonText.text).GenerateDescryption(), ResourcesManager.Instance.ColorPalette["pink"]);
        }
        else {
            Descryption.text = TextUtility.ColorText(SelectedCharacter.BasicAttack.GenerateDescryption(), ResourcesManager.Instance.ColorPalette["pink"]);
        }
    }

    public void OnPointerExit(PointerEventData eventData) {
        Descryption.text = string.Empty;
    }

}