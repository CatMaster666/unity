﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class UIView : MonoBehaviour {

    private TextMeshProUGUI descryption;
    [SerializeField] private GameObject abilityButton;
    [SerializeField] private GameObject attackButton;
    [SerializeField] private GameObject moveButton;
    [SerializeField] private GameObject spellButtonPrefab;
    [SerializeField] private GameObject spellButtonsSpawn;
    private List<GameObject> spellButtons = new List<GameObject>();

    private GameController controller;

    private void Start() {
        descryption = GameObject.Find("Descryption").GetComponent<TextMeshProUGUI>();
        controller = FindObjectOfType<GameController>();
    }

    public void UpdateUnitUi() {
        Character selected = ResourcesManager.Instance.Data.GetSelecetedUnit();

        if (selected == null) {
            moveButton.SetActive(false);
            attackButton.SetActive(false);
            abilityButton.SetActive(false);
        }
        else {
            moveButton.SetActive(true);
            abilityButton.SetActive(true);
            attackButton.SetActive(true);

            if (ResourcesManager.Instance.Data.GameState == State.SelectingAbility ||
                ResourcesManager.Instance.Data.GameState == State.Cast) {

                spellButtonsSpawn.SetActive(true);

                while (spellButtons.Count != selected.AvailibleAbilities.Count) {
                    if (spellButtons.Count < selected.AvailibleAbilities.Count) {
                        GameObject button = Instantiate(spellButtonPrefab, spellButtonsSpawn.transform);
                        spellButtons.Add(button);
                    }

                    if (spellButtons.Count > selected.AvailibleAbilities.Count) {
                        DestroyImmediate(spellButtons[spellButtons.Count - 1]);
                        spellButtons.RemoveAt(spellButtons.Count - 1);
                    }

                }

                for (int i = 0; i < spellButtons.Count; i++) {
                    int index = i;
                    GameObject button = spellButtons[i];
                    button.GetComponentInChildren<TextMeshProUGUI>().text = selected.AvailibleAbilities[i].Name;
                    Button buttonComponent = button.GetComponent<Button>();
                    buttonComponent.onClick.RemoveAllListeners();
                    buttonComponent.onClick.AddListener(() => controller.SetSelectedAbility(selected.AvailibleAbilities[index].Name));
                }
            }
            else {
                spellButtonsSpawn.SetActive(false);
            }
        }
    }

    public void UpdateDescryption() {

        Unit unit = ResourcesManager.Instance.Data.GetUnitAt(BoardData.MouseToBoardPosition());
        if (unit == null) {
            descryption.text = string.Empty;
            return;
        }
        if (unit is Character) {
            Character character = unit as Character;
            descryption.text = TextUtility.ColorText(character.GetFullDescryption(), !character.Player
                                                         ? ResourcesManager.Instance.ColorPalette["green"]
                                                         : ResourcesManager.Instance.ColorPalette["blue"]);
        }
        else if (unit.IsSummon) {
            descryption.text = unit.ToString();
        }
        else {
            descryption.text = string.Empty;
        }

    }

}