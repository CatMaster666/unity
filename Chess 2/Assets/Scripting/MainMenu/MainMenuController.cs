﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenuController : MonoBehaviour {

    public void QuitButton() {
        Application.Quit();
    }

    public void LoadButton() {
        ResourcesManager.Instance.Data.Load();
        SceneManager.LoadScene("Game");
    }

    public void OptionsButton() {

    }

    public void NewGameButton() {
        SceneManager.LoadScene("GameCreation");
    }

    public void SaveButton() {
        ResourcesManager.Instance.Data.Save();
    }

    public void MainMenuButton() {
        Destroy(GameObject.Find("PersistentObject"));
        SceneManager.LoadScene("MainMenu");
    }

}