﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class TeamCreationView : MonoBehaviour {

    private TeamCreator creator;
    private List<TeamMemberButton> TeamButtons = new List<TeamMemberButton>();

    [SerializeField] private GameObject teamAButtonPrefab;
    [SerializeField] private GameObject teamBButtonPrefab;
    private Transform teamAButtonsSpawn;
    private Transform teamBButtonsSpawn;

    private void Start() {

        DontDestroyOnLoad(gameObject);
    }

    public void setTeamSizeClick(int size) {
        StartCoroutine(AfterTeamSizeWasSet(size));
    }

    private IEnumerator AfterTeamSizeWasSet(int size) {
        SceneManager.LoadScene("TeamCreation");
        yield return null;
        creator = new TeamCreator();
        creator.SetTeamSize(size);
        Init(size);

    }

    private void Init(int teamSize) {
        GameObject.Find("Play").GetComponent<Button>().onClick.AddListener(Play);
        teamAButtonsSpawn = GameObject.Find("SpawnA").transform;
        teamBButtonsSpawn = GameObject.Find("SpawnB").transform;

        for (int i = 0; i < teamSize; i++) {
            InstantiateTeamMemberButton(true, i);
            InstantiateTeamMemberButton(false, i);
        }

    }

    private void InstantiateTeamMemberButton(bool team, int index) {
        TeamMemberButton teamButton = team
            ? Instantiate(teamAButtonPrefab, teamAButtonsSpawn).GetComponent<TeamMemberButton>()
            : Instantiate(teamBButtonPrefab, teamBButtonsSpawn).GetComponent<TeamMemberButton>();
        teamButton.Init(team, index, creator);
        teamButton.button.onClick.AddListener(() => { teamButtonClick(team, teamButton); });
        TeamButtons.Add(teamButton);
    }

    private void teamButtonClick(bool team, TeamMemberButton button) {

        HideAllClassButtons();
        button.ShowClassButtons();

    }

    private void HideAllClassButtons() {
        foreach (TeamMemberButton button in TeamButtons) {
            button.HideClassButtons();
        }
    }

    private void Play() {

        if (creator.AreTeamsComplete()) {
            creator.Play();
            Destroy(gameObject);
        }
        else {
            Debug.Log("not complete");
        }

    }

}