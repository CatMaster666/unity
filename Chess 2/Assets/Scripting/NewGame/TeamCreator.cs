﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class TeamCreator {

    private Character[] TeamA;
    private Character[] TeamB;
    private PathfindingModel pathfinder;

    public TeamCreator() {
        ResourcesManager.Instance.Data.Units = new List<Unit>();
        pathfinder = new PathfindingModel(ResourcesManager.Instance.Data);
    }

    public void SetTeamSize(int size) {
        TeamA = new Character[size];
        TeamB = new Character[size];
    }

    public void SetTeamMember(bool team, int index, CharacterClass charactercClass) {
        if (team) {
            TeamA[index] = new Character(charactercClass, team);
        }
        else {
            TeamB[index] = new Character(charactercClass, team);
        }
    }

    public bool AreTeamsComplete() {
        if (TeamA.Contains(null)) {
            return false;
        }
        return !TeamB.Contains(null);
    }

    public void Play() {

        SpawnObstaclesAndTestWalkability(60);

        foreach (Character character in TeamA) {
            character.Position = BoardData.GetRandomFreePosition();
            ResourcesManager.Instance.Data.Units.Add(character);
        }
        foreach (Character character in TeamB) {
            character.Position = BoardData.GetRandomFreePosition();
            ResourcesManager.Instance.Data.Units.Add(character);
        }
        SceneManager.LoadScene("Game");

    }

    private void SpawnObstaclesAndTestWalkability(int obstacles) {

        ResourcesManager.Instance.Data.Units.RemoveAll(x => !(x is Character));

        for (int i = 0; i < obstacles; i++) {
            Unit U = new Unit();
            U.InitUnit(0, 0, "CrateSprite", false, 0);
            U.Position = BoardData.GetRandomFreePosition();
            ResourcesManager.Instance.Data.Units.Add(U);
        }

        pathfinder.CalculatePathfindingGraph(BoardData.GetRandomFreePosition());
        for (int x = 0; x < BoardData.Width; x++) {
            for (int y = 0; y < BoardData.Height; y++) {
                if (ResourcesManager.Instance.Data.IsFree(x, y)) {
                    if (!pathfinder.IsReachable(new Vector2(x, y))) {
                        SpawnObstaclesAndTestWalkability(obstacles - 1);
                    }
                }
            }
        }
    }

}