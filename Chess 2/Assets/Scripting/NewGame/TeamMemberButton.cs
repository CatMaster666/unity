﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class TeamMemberButton : MonoBehaviour {

    private TeamCreator creator;
    private bool team;
    private int index;

    public Button button;
    private Image characterImage;
    private Image backgroundImage;
    private Transform classButtonsSpawn;
    [SerializeField] private GameObject classButtonPrefab;

    public void Init(bool buttonTeam, int buttonIndex, TeamCreator teamCreator) {
        team = buttonTeam;
        index = buttonIndex;
        creator = teamCreator;

        button = GetComponent<Button>();
        characterImage = GetComponentsInChildren<Image>()[1];
        backgroundImage = GetComponent<Image>();
        classButtonsSpawn = transform.GetChild(1).GetChild(0);

        backgroundImage.color = team
            ? ResourcesManager.Instance.ColorPalette["green"]
            : ResourcesManager.Instance.ColorPalette["blue"];

        Color colorToModify = characterImage.color;
        colorToModify.a = 0;
        characterImage.color = colorToModify;

        AddClassButton(CharacterClass.Warrior);
        AddClassButton(CharacterClass.Medic);
        AddClassButton(CharacterClass.Mage);
        AddClassButton(CharacterClass.Engineer);

        HideClassButtons();

    }

    private void AddClassButton(CharacterClass characterClass) {
        Button classButton = Instantiate(classButtonPrefab, classButtonsSpawn).GetComponent<Button>();
        classButton.GetComponentInChildren<TextMeshProUGUI>().text = characterClass.ToString();

        classButton.onClick.AddListener(() => {
            ClassButtonClick(characterClass);
            Color colorToModify = characterImage.color;
            colorToModify.a = 1;
            characterImage.color = colorToModify;
        });
    }

    private void ClassButtonClick(CharacterClass characterClass) {
        HideClassButtons();
        SetImage(characterClass);
        creator.SetTeamMember(team, index, characterClass);
    }

    private void SetImage(CharacterClass characterClass) {
        characterImage.sprite = ResourcesManager.Instance.Sprites[characterClass + "Sprite"];
    }

    public void HideClassButtons() {
        classButtonsSpawn.parent.gameObject.SetActive(false);
    }

    public void ShowClassButtons() {
        classButtonsSpawn.parent.gameObject.SetActive(true);
    }

}