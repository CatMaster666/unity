﻿using UnityEngine;
using NUnit.Framework;
using System.Collections.Generic;


public class Pathfinding_Tests_Not_Empty_Board {

    BoardData boardData = new BoardData();
    private PathfindingModel pathfinder;

    [OneTimeSetUp]
    public void Init() {
        boardData.Units.Add(new Unit {Duration = 0, IsSummon = false, Position = new Vector2(8, 9), SpriteName = ""});
        boardData.Units.Add(new Unit {Duration = 0, IsSummon = false, Position = new Vector2(8, 8), SpriteName = ""});
        boardData.Units.Add(new Unit {Duration = 0, IsSummon = false, Position = new Vector2(8, 7), SpriteName = ""});
        boardData.Units.Add(new Unit {Duration = 0, IsSummon = false, Position = new Vector2(8, 6), SpriteName = ""});
        boardData.Units.Add(new Unit {Duration = 0, IsSummon = false, Position = new Vector2(9, 6), SpriteName = ""});
        boardData.Units.Add(new Unit {Duration = 0, IsSummon = false, Position = new Vector2(8, 5), SpriteName = ""});
        boardData.Units.Add(new Unit {Duration = 0, IsSummon = false, Position = new Vector2(8, 4), SpriteName = ""});
        boardData.Units.Add(new Unit {Duration = 0, IsSummon = false, Position = new Vector2(8, 3), SpriteName = ""});
        boardData.Units.Add(new Unit {Duration = 0, IsSummon = false, Position = new Vector2(8, 5), SpriteName = ""});
        boardData.Units.Add(new Unit {Duration = 0, IsSummon = false, Position = new Vector2(8, 4), SpriteName = ""});
        boardData.Units.Add(new Unit {Duration = 0, IsSummon = false, Position = new Vector2(8, 3), SpriteName = ""});
        pathfinder = new PathfindingModel(boardData);
    }

    [Test]
    public void GetPath_In_Range_Is_Not_Empty() {

        pathfinder.CalculatePathfindingGraph(new Vector2(2, 2));
        Assert.IsNotEmpty(pathfinder.GetPath(new Vector2(3, 4), 3));
    }

    [Test]
    public void GetPath_Not_In_Range_Is_Empty() {

        pathfinder.CalculatePathfindingGraph(new Vector2(2, 2));
        Assert.IsEmpty(pathfinder.GetPath(new Vector2(7, 7), 5));
    }

    [Test]
    public void GetPath_Outside_Of_The_BoardSize_Is_Empty() {
        pathfinder.CalculatePathfindingGraph(new Vector2(1000, 1000));
        Assert.IsEmpty(pathfinder.GetPath(new Vector2(1000, 1005), 6));
    }

    [Test]
    public void GetPaths_Zero_Range_Is_Empty() {
        pathfinder.CalculatePathfindingGraph(new Vector2(3, 3));
        Assert.IsEmpty(pathfinder.GetValidPaths(0));
    }

    [Test]
    public void GetPath_From_Invalid_Position_Is_Not_Empty() {
        pathfinder.CalculatePathfindingGraph(new Vector2(8, 8));
        Assert.IsNotEmpty(pathfinder.GetPath(new Vector2(3, 3), 40));
    }

    [Test]
    public void GetPath_To_Invalid_Position_Is_Empty() {
        pathfinder.CalculatePathfindingGraph(new Vector2(4, 4));
        Assert.IsEmpty(pathfinder.GetPath(new Vector2(8, 8), 40));
        Assert.IsEmpty(pathfinder.GetPath(new Vector2(9, 8), 40));
    }

    [Test]
    public void GetPath_To_Valid_Position() {
        pathfinder.CalculatePathfindingGraph(new Vector2(7, 3));
        List<Vector2> path = pathfinder.GetPath(new Vector2(9, 3), 4);

        Assert.IsTrue(path.Contains(new Vector2(7, 2)));
        Assert.IsTrue(path.Contains(new Vector2(8, 2)));
        Assert.IsTrue(path.Contains(new Vector2(9, 2)));
        Assert.IsTrue(path.Contains(new Vector2(9, 3)));

    }

    [Test]
    public void GetPaths_Valid() {
        pathfinder.CalculatePathfindingGraph(new Vector2(5, 5));
        List<Vector2> Paths = pathfinder.GetValidPaths(2);
        Assert.IsTrue(Paths.Contains(new Vector2(5, 5)));
        Assert.IsTrue(Paths.Contains(new Vector2(6, 5)));
        Assert.IsTrue(Paths.Contains(new Vector2(7, 5)));
        Assert.IsTrue(Paths.Contains(new Vector2(6, 6)));
        Assert.IsTrue(Paths.Contains(new Vector2(5, 6)));
        Assert.IsTrue(Paths.Contains(new Vector2(5, 7)));
        Assert.IsTrue(Paths.Contains(new Vector2(4, 6)));
        Assert.IsTrue(Paths.Contains(new Vector2(4, 5)));
        Assert.IsTrue(Paths.Contains(new Vector2(3, 5)));
        Assert.IsTrue(Paths.Contains(new Vector2(4, 4)));
        Assert.IsTrue(Paths.Contains(new Vector2(5, 4)));
        Assert.IsTrue(Paths.Contains(new Vector2(5, 3)));
        Assert.IsTrue(Paths.Contains(new Vector2(6, 4)));
    }

    [Test]
    public void IsReachable_Valid_Position() {
        pathfinder.CalculatePathfindingGraph(new Vector2(4, 4));
        Assert.IsTrue(pathfinder.IsReachable(new Vector2(6, 7)));
    }

    [Test]
    public void IsReachable_Invalid_Position() {
        pathfinder.CalculatePathfindingGraph(new Vector2(4, 4));
        Assert.IsFalse(pathfinder.IsReachable(new Vector2(8, 8)));
        Assert.IsFalse(pathfinder.IsReachable(new Vector2(9, 8)));
    }

    [Test]
    public void IsReachable_OutSide_Board() {
        pathfinder.CalculatePathfindingGraph(new Vector2(4, 4));
        Assert.IsFalse(pathfinder.IsReachable(new Vector2(20, 20)));
        Assert.IsFalse(pathfinder.IsReachable(new Vector2(-1, 5)));
    }

}