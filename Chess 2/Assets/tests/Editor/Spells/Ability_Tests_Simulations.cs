﻿using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;


public class Ability_Tests : MonoBehaviour {

    private BoardData boardData = new BoardData();
    private Character mage;
    private Character warrior;
    private Character warrior2;
    private Character engineer;
    private Character medic;

    [OneTimeSetUp]
    public void InitResources() {

        ResourcesManager.Instance.Init();
    }

    [SetUp]
    public void Init() {

        mage = new Character(CharacterClass.Mage, true);
        warrior = new Character(CharacterClass.Warrior, true);
        warrior2 = new Character(CharacterClass.Warrior, true);
        engineer = new Character(CharacterClass.Engineer, false);
        medic = new Character(CharacterClass.Medic, false);
        mage.Position = new Vector2(5, 5);
        warrior.Position = new Vector2(6, 5);
        warrior2.Position = new Vector2(2, 2);
        engineer.Position = new Vector2(7, 7);
        medic.Position = new Vector2(8, 7);

        boardData.Units.Add(mage);
        boardData.Units.Add(warrior);
        boardData.Units.Add(warrior2);
        boardData.Units.Add(engineer);
        boardData.Units.Add(medic);

    }

    [TearDown]
    public void DeInit() {
        boardData.Units.Clear();
        boardData.Graveyard.Clear();

    }

///////////////////////////////////////////Basic Attack////////////////////////////////////

    [Test]
    public void BasicAttack_Non_Lethal() {

        List<Vector2> targets = new List<Vector2> {mage.Position};
        warrior.BasicAttack.Use(targets, warrior, boardData);

        Assert.AreEqual(25, mage.HitPoints);
        Assert.AreEqual(70, warrior.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void BasicAttack_Non_Lethal_And_Reverse_It() {
        List<Vector2> targets = new List<Vector2> {mage.Position};
        warrior.BasicAttack.Use(targets, warrior, boardData);
        warrior.BasicAttack.Undo();
        Assert.AreEqual(30, mage.HitPoints);
        Assert.AreEqual(70, warrior.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void Multiple_BasicAttacks_Non_Lethal() {
        List<Vector2> targets = new List<Vector2> {mage.Position};
        for (int i = 0; i < 3; i++) {
            warrior.BasicAttack.Use(targets, warrior, boardData);
        }
        Assert.AreEqual(15, mage.HitPoints);
        Assert.AreEqual(70, warrior.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void Multiple_BasicAttacks_Non_Lethal_Reverse_All() {
        List<Vector2> targets = new List<Vector2> {mage.Position};
        for (int i = 0; i < 3; i++) {
            warrior.BasicAttack.Use(targets, warrior, boardData);
        }
        for (int i = 0; i < 3; i++) {

            warrior.BasicAttack.Undo();
        }
        Assert.AreEqual(30, mage.HitPoints);
        Assert.AreEqual(70, warrior.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void Multiple_BasicAttacs_Non_Lethal_Reverse_Each_One_Imediatly() {
        List<Vector2> targets = new List<Vector2> {mage.Position};
        for (int i = 0; i < 3; i++) {
            warrior.BasicAttack.Use(targets, warrior, boardData);
            warrior.BasicAttack.Undo();
        }
        Assert.AreEqual(30, mage.HitPoints);
        Assert.AreEqual(70, warrior.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void BasicAttack_Lethal() {
        List<Vector2> targets = new List<Vector2> {mage.Position};

        mage.HitPoints = 5;
        warrior.BasicAttack.Use(targets, warrior, boardData);
        Assert.True(!boardData.Units.Contains(mage));
        Assert.True(boardData.Graveyard.Contains(mage));
        Assert.AreEqual(0, mage.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void BasicAttack_Lethal_Reverse_It() {
        List<Vector2> targets = new List<Vector2> {mage.Position};

        mage.HitPoints = 5;
        warrior.BasicAttack.Use(targets, warrior, boardData);
        warrior.BasicAttack.Undo();

        Assert.True(boardData.Units.Contains(mage));
        Assert.True(!boardData.Graveyard.Contains(mage));
        Assert.AreEqual(5, mage.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void Multiple_BasicAttacks_Last_Is_Lethal_Reverse_All() {
        List<Vector2> targets = new List<Vector2> {mage.Position};

        mage.HitPoints = 11;
        for (int i = 0; i < 3; i++) {
            warrior.BasicAttack.Use(targets, warrior, boardData);
        }
        for (int i = 0; i < 3; i++) {
            warrior.BasicAttack.Undo();
        }
        Assert.True(boardData.Units.Contains(mage));
        Assert.True(!boardData.Graveyard.Contains(mage));
        Assert.AreEqual(11, mage.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void Multiple_BasicAttacks_Some_After_Lethal() {
        List<Vector2> targets = new List<Vector2> {mage.Position};

        mage.HitPoints = 6;
        for (int i = 0; i < 6; i++) {
            warrior.BasicAttack.Use(targets, warrior, boardData);
        }
        Assert.True(!boardData.Units.Contains(mage));
        Assert.True(boardData.Graveyard.Contains(mage));
        Assert.AreEqual(-4, mage.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

////////////////////////////////////////////////Fireball/////////////////////////////////////

    [Test]
    public void Single_Fireball_Non_Lethal() {
        List<Vector2> targets = new List<Vector2> {warrior.Position};

        mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Use(targets, mage, boardData);
        Assert.IsTrue(warrior.AppliedEffects.Find(e => e.EffectTag == DurationEffect.DurationEffectTag.Burning) != null);
        Assert.AreEqual(55, warrior.HitPoints);
        Assert.AreEqual(30, mage.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void Single_Fireball_Non_Lethal_Reverse_It() {
        List<Vector2> targets = new List<Vector2> {warrior.Position};

        mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Use(targets, mage, boardData);
        mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Undo();
        Assert.IsTrue(warrior.AppliedEffects.Find(e => e.EffectTag == DurationEffect.DurationEffectTag.Burning) == null);
        Assert.True(boardData.Units.Contains(warrior));
        Assert.AreEqual(70, warrior.HitPoints);
        Assert.AreEqual(30, mage.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void Multiple_Fireballs_Non_Lethal() {
        List<Vector2> targets = new List<Vector2> {warrior.Position};

        for (int i = 0; i < 3; i++) {
            mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Use(targets, mage, boardData);
        }
        Assert.IsTrue(warrior.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Burning).Count == 3);
        Assert.AreEqual(30, mage.HitPoints);
        Assert.AreEqual(25, warrior.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void Multiple_Fireballs_Non_Lethal_Reverse_Last() {
        List<Vector2> targets = new List<Vector2> {warrior.Position};

        for (int i = 0; i < 3; i++) {
            mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Use(targets, mage, boardData);
        }
        mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Undo();
        Assert.IsTrue(warrior.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Burning).Count == 2);
        Assert.AreEqual(30, mage.HitPoints);
        Assert.AreEqual(40, warrior.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void Multiple_Fireballs_Non_Lethal_Reverse_All() {
        List<Vector2> targets = new List<Vector2> {warrior.Position};

        for (int i = 0; i < 3; i++) {
            mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Use(targets, mage, boardData);

        }
        for (int i = 0; i < 3; i++) {
            mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Undo();
        }
        Assert.IsTrue(warrior.AppliedEffects.Find(e => e.EffectTag == DurationEffect.DurationEffectTag.Burning) == null);
        Assert.AreEqual(30, mage.HitPoints);
        Assert.AreEqual(70, warrior.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void MultipleTargets_Single_Fireball_Non_Lethal_Reverse_All() {
        warrior2.HitPoints = 60;
        List<Vector2> targets = new List<Vector2> {warrior.Position, warrior2.Position};
        mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Use(targets, mage, boardData);
        mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Undo();

        Assert.IsTrue(warrior.AppliedEffects.Find(e => e.EffectTag == DurationEffect.DurationEffectTag.Burning) == null);
        Assert.IsTrue(warrior2.AppliedEffects.Find(e => e.EffectTag == DurationEffect.DurationEffectTag.Burning) == null);

        Assert.AreEqual(70, warrior.HitPoints);
        Assert.AreEqual(60, warrior2.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    [Test]
    public void MultipleTargets_Multiple_Fireballs_One_Target_Dies_Reverse_All() {
        warrior2.HitPoints = 40;
        warrior.HitPoints = 70;
        List<Vector2> targets = new List<Vector2> {warrior.Position, warrior2.Position};

        for (int i = 0; i < 3; i++) {
            mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Use(targets, mage, boardData);
        }

        Assert.AreEqual(3, warrior.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Burning).Count);
        Assert.AreEqual(2, warrior2.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Burning).Count);
        Assert.AreEqual(25, warrior.HitPoints);
        Assert.AreEqual(-5, warrior2.HitPoints);

        for (int i = 0; i < 3; i++) {
            mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Undo();
        }

        Assert.AreEqual(0, warrior.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Burning).Count);
        Assert.AreEqual(0, warrior2.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Burning).Count);
        Assert.AreEqual(70, warrior.HitPoints);
        Assert.AreEqual(40, warrior2.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

////////////////////////////////////////////////////////More Complex tests of different abilities and interactions////////////////////////////////////////////////

    [Test]
    public void Complex_Situation_Reverse_All() {

        mage.HitPoints = 10;
        warrior.HitPoints = 70;
        warrior2.HitPoints = 30;
        engineer.HitPoints = 40;
        medic.HitPoints = 45;

        List<Vector2> fireballTargets1 = new List<Vector2> {warrior.Position, warrior2.Position, engineer.Position};
        List<Vector2> fireballTargets2 = new List<Vector2> {warrior.Position, medic.Position, warrior2.Position};
        List<Vector2> blinkTarget = new List<Vector2> {new Vector2(8, 8)};

        mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Use(fireballTargets1, mage, boardData);
        mage.AvailibleAbilities.Find(a => a.Name == "Blink").Use(blinkTarget, mage, boardData);
        engineer.AvailibleAbilities.Find(a => a.Name == "Taser").Use(new List<Vector2> {warrior.Position}, engineer, boardData);
        mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Use(fireballTargets2, mage, boardData);
        medic.AvailibleAbilities.Find(a => a.Name == "Heal").Use(fireballTargets2, medic, boardData);

        medic.AvailibleAbilities.Find(a => a.Name == "Heal").Undo();
        mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Undo();
        engineer.AvailibleAbilities.Find(a => a.Name == "Taser").Undo();
        mage.AvailibleAbilities.Find(a => a.Name == "Blink").Undo();
        mage.AvailibleAbilities.Find(a => a.Name == "Fireball").Undo();

        Assert.AreEqual(10, mage.HitPoints);
        Assert.AreEqual(70, warrior.HitPoints);
        Assert.AreEqual(30, warrior2.HitPoints);
        Assert.AreEqual(40, engineer.HitPoints);
        Assert.AreEqual(45, medic.HitPoints);

        Assert.AreEqual(0, warrior.AppliedEffects.Count);
        Assert.AreEqual(0, warrior2.AppliedEffects.Count);
        Assert.AreEqual(0, mage.AppliedEffects.Count);
        Assert.AreEqual(0, engineer.AppliedEffects.Count);
        Assert.AreEqual(0, medic.AppliedEffects.Count);

        Assert.AreEqual(new Vector2(5, 5), mage.Position);
        Assert.AreEqual(new Vector2(6, 5), warrior.Position);
        Assert.AreEqual(new Vector2(2, 2), warrior2.Position);
        Assert.AreEqual(new Vector2(7, 7), engineer.Position);
        Assert.AreEqual(new Vector2(8, 7), medic.Position);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }

    /*[Test]
    public void DurationEffect_Multiple_Targets_Reverse() {

        List<Vector2> virusTargets = new List<Vector2> {warrior.Position, warrior2.Position, engineer.Position};
        medic.AvailibleAbilities.Find(a => a.Name == "Virus").Use(virusTargets, medic, boardData);
        medic.AvailibleAbilities.Find(a => a.Name == "Virus").Use(virusTargets, medic, boardData);

        Assert.AreEqual(1, warrior.HitPoints);
        Assert.AreEqual(1, warrior2.HitPoints);
        Assert.AreEqual(1, engineer.HitPoints);
        Assert.AreEqual(2, warrior.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Virus).Count);
        Assert.AreEqual(2, warrior2.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Virus).Count);
        Assert.AreEqual(2, engineer.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Virus).Count);

        medic.AvailibleAbilities.Find(a => a.Name == "Virus").Undo();
        medic.AvailibleAbilities.Find(a => a.Name == "Virus").Undo();

        Assert.AreEqual(70, warrior.HitPoints);
        Assert.AreEqual(70, warrior2.HitPoints);
        Assert.AreEqual(40, engineer.HitPoints);
        Assert.AreEqual(0, warrior.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Virus).Count);
        Assert.AreEqual(0, warrior2.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Virus).Count);
        Assert.AreEqual(0, engineer.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Virus).Count);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;
    }*/

    [Test]
    public void DurationEffect_Single_Target_Reverse_Serialization() {
        List<Vector2> novaTargets = new List<Vector2> {warrior.Position};

        //mage.AvailibleAbilities.Find(a => a.Name == "BasicAttack").Use(novaTargets, mage, boardData);

        //mage.AvailibleAbilities.Find(a => a.Name == "BasicAttack").Undo();

        //Assert.AreEqual(1, warrior.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Slowed).Count);
        //Assert.AreEqual(1, warrior.Statistics[StatisticType.MovementRange].Value);
        //Assert.AreEqual(50, warrior.HitPoints);

        string save = JsonSerializer.Serialize(typeof(BoardData), boardData);
        boardData = JsonSerializer.Deserialize(typeof(BoardData), save) as BoardData;

        /*Assert.AreEqual(1, warrior.AppliedEffects.FindAll(e => e.EffectTag == DurationEffect.DurationEffectTag.Slowed).Count);
        Assert.AreEqual(1, warrior.Statistics[StatisticType.MovementRange].Value);
        Assert.AreEqual(50, warrior.HitPoints);*/
    }

}